package qa.gov.hukoomi.hukoomimobile.payment_history.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_payment_history.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryModel
import qa.gov.hukoomi.hukoomimobile.payment_history.model.PaymentHistoryModel

class PaymentHistoryAdapter(val context: Context,
                            private val paymentHistoryModel: ArrayList<PaymentHistoryModel>) :
    RecyclerView.Adapter<PaymentHistoryAdapter.MyViewHolder>() {

    var currentPosition = 0

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var layout = view.layout
        var paymentReceiptAndAmount = view.paymentReceiptAndAmount
        var arrow = view.arrow

        var expandedLayout = view.expandedLayout
        var trxDate = view.trxDate
        var paymentStatus = view.paymentStatus
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_payment_history, parent, false)

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.paymentReceiptAndAmount.text = "${paymentHistoryModel.get(position).receiptNum} - ${paymentHistoryModel.get(position).amount} QR"
        holder.trxDate.text = paymentHistoryModel.get(position).trxDate
        holder.paymentStatus.text = paymentHistoryModel.get(position).paymentStatus
        holder.expandedLayout.visibility = View.GONE

        holder.layout.setBackgroundColor(context.resources.getColor(R.color.colorAccent))
        holder.paymentReceiptAndAmount.setTextColor(context.resources.getColor(R.color.dashboard_text_color))
        Picasso.get().load(R.mipmap.ic_expand).into(holder.arrow)

        if (currentPosition == position) {
            holder.expandedLayout.visibility = View.VISIBLE
            holder.layout.setBackgroundColor(context.resources.getColor(R.color.colorPrimary))
            holder.paymentReceiptAndAmount.setTextColor(context.resources.getColor(R.color.colorAccent))
            Picasso.get().load(R.mipmap.ic_collapse).into(holder.arrow)
        }

        holder.layout.setOnClickListener {
            currentPosition = position
            notifyDataSetChanged()
        }
    }

    override fun getItemCount() = paymentHistoryModel.size

    interface ItemClick {
        fun item_click(position: Int, directoryName: DirectoryModel)
    }
}