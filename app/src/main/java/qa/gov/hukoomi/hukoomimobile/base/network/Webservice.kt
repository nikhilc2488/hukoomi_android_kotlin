package qa.gov.hukoomi.hukoomimobile.base.network

import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryDetailResponse
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardCheckExpiryModel
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardCheckExpiryResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.LoginModel
import qa.gov.hukoomi.hukoomimobile.login.model.LoginResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.OTPResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.VerifyOTPModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface Webservice {

    //login webservice
    @POST("sf/login")
    fun login(@Body loginModel: LoginModel): Call<OTPResponseModel>

    //otp webservice
    @POST("sf/verifyotp")
    fun verifyOTP(@Body verifyOTPModel: VerifyOTPModel): Call<LoginResponseModel>

    //checkHealthCardExpiry webservice
    @POST("healthcard/basicinfo")
    fun checkHealthCardExpiry(@Body checkExpiryModel: HealthCardCheckExpiryModel): Call<HealthCardCheckExpiryResponseModel>
    
}