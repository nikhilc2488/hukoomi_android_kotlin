package qa.gov.hukoomi.hukoomimobile.about.fragment

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.fragment_about_us.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.about.adapter.AboutUsAdapter
import qa.gov.hukoomi.hukoomimobile.about.model.AboutUsModel
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment

class AboutUsFragment: BaseMainFragment() {

    private lateinit var rvAboutUs: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun getLayoutId(): Int {
        return R.layout.fragment_about_us
    }

    override fun initializeViews() {

        getMainActivity()?.setDashboardVisibility(View.GONE)
        getMainActivity()?.setTitleVisibility(View.VISIBLE)

        val itemList = arrayListOf<AboutUsModel>()
        itemList.add(AboutUsModel(resources.getString(R.string.location_and_geography), resources.getString(R.string.location_and_geography_desc)))
        itemList.add(AboutUsModel(resources.getString(R.string.capital_city), resources.getString(R.string.capital_city_desc)))
        itemList.add(AboutUsModel(resources.getString(R.string.religion), resources.getString(R.string.religion_desc)))
        itemList.add(AboutUsModel(resources.getString(R.string.language), resources.getString(R.string.language_desc)))
        itemList.add(AboutUsModel(resources.getString(R.string.currency), resources.getString(R.string.currency_desc)))
        itemList.add(AboutUsModel(resources.getString(R.string.local_time), resources.getString(R.string.local_time_desc)))
        itemList.add(AboutUsModel(resources.getString(R.string.official_working_hours), resources.getString(R.string.official_working_hours_desc)))
        itemList.add(AboutUsModel(resources.getString(R.string.the_national_day), resources.getString(R.string.the_national_day_desc)))
        itemList.add(AboutUsModel(resources.getString(R.string.national_sport_day), resources.getString(R.string.national_sport_day_desc)))

        viewManager = LinearLayoutManager(getMainActivity()?.applicationContext)
        viewAdapter = AboutUsAdapter(itemList)

        rvAboutUs = rv_aboutUs.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

        }
    }

    override fun setListeners() {
        //no listeners
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.about_us)
    }

    override fun serviceIcon(): Int {
        return R.mipmap.ic_about_us
    }

    override fun backBtnVisibility(): Int {
        return View.VISIBLE
    }
}