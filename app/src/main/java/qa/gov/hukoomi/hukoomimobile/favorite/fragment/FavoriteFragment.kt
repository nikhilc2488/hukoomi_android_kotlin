package qa.gov.hukoomi.hukoomimobile.favorite.fragment

import android.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_favorite.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment
import qa.gov.hukoomi.hukoomimobile.favorite.adapter.FavoriteAdapter
import qa.gov.hukoomi.hukoomimobile.favorite.model.FavoriteModel
import qa.gov.hukoomi.hukoomimobile.general.fragment.DirectoryFragment
import qa.gov.hukoomi.hukoomimobile.healthcard.fragment.HealthCardEServiceFragment
import qa.gov.hukoomi.hukoomimobile.utils.get
import qa.gov.hukoomi.hukoomimobile.utils.put
import qa.gov.hukoomi.hukoomimobile.utils.toast

class FavoriteFragment : BaseMainFragment(), FavoriteAdapter.ItemClick{

    private lateinit var rvFavorite: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    var favoriteList = ArrayList<FavoriteModel>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_favorite
    }

    override fun initializeViews() {
        getMainActivity()?.setDashboardVisibility(View.GONE)
        getMainActivity()?.setTitleVisibility(View.VISIBLE)

        val healthCardEServiceJSON = getMainActivity()?.getPreference()!!.get(resources.getString(R.string.health_card_e_services), "")
        val favoriteModel_one = Gson().fromJson<FavoriteModel>(healthCardEServiceJSON, FavoriteModel::class.java)

        val generalJson = getMainActivity()?.getPreference()!!.get(resources.getString(R.string.directory), "")
        val favoriteModel_two = Gson().fromJson<FavoriteModel>(generalJson, FavoriteModel::class.java)

        favoriteList.clear()
        if (favoriteModel_one != null) {
            favoriteList.add(favoriteModel_one)
        }
        if (favoriteModel_two != null) {
            favoriteList.add(favoriteModel_two)
        }

        viewManager = LinearLayoutManager(getMainActivity()?.applicationContext)
        viewAdapter = FavoriteAdapter(favoriteList, this)

        rvFavorite = rv_favorite.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

        }
    }

    override fun setListeners() {
        //no listeners
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.favorites)
    }

    override fun serviceIcon(): Int {
        return R.mipmap.ic_fav_title
    }

    override fun backBtnVisibility(): Int {
        return View.GONE
    }

    override fun itemClick(position: Int, favoriteModel: FavoriteModel) {
        when (favoriteModel.favoriteServiceName) {
            resources.getString(R.string.health_card_e_services) -> {
                getMainActivity()?.addFragment(HealthCardEServiceFragment())
            }
            resources.getString(R.string.directory) -> {
                getMainActivity()?.addFragment(DirectoryFragment())
            }
        }
    }

    override fun itemLongClick(position: Int, favoriteModel: FavoriteModel): Boolean {
        when (favoriteModel.favoriteServiceName) {
            resources.getString(R.string.health_card_e_services) -> {
                return showDialog(position)
            }
            resources.getString(R.string.directory) -> {
                return showDialog(position)
            }
        }
        return false
    }

    fun showDialog(position: Int): Boolean {
        val builder = AlertDialog.Builder(getMainActivity())
        builder.setTitle(resources.getString(R.string.remove_from_favorites))
        builder.setMessage(resources.getString(R.string.remove_from_favorites_message))

        builder.setPositiveButton(resources.getString(R.string.yes)) { dialog, which ->
            getMainActivity()?.getPreference()?.put(favoriteList.get(position).favoriteServiceName, "")
            favoriteList.removeAt(position)
            viewAdapter.notifyDataSetChanged()
            getMainActivity()?.toast(resources.getString(R.string.service_removed_from_favorite))
            dialog.dismiss()
        }

        builder.setNegativeButton(resources.getString(R.string.no)) { dialog, which ->
            dialog.dismiss()
        }

        val dialog: AlertDialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.colorPrimary))
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.colorPrimary))
        }
        dialog.show()
        return true
    }
}