package qa.gov.hukoomi.hukoomimobile.services.fragment

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.fragment_services_one.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment
import qa.gov.hukoomi.hukoomimobile.general.fragment.GeneralFragment
import qa.gov.hukoomi.hukoomimobile.healthcard.fragment.HealthServiceFragment
import qa.gov.hukoomi.hukoomimobile.services.adapter.ServicesAdapter
import qa.gov.hukoomi.hukoomimobile.services.model.ServiceModel

class ServicesFragment : BaseMainFragment(), ServicesAdapter.ItemClick {

    private lateinit var rvServices: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    val servicesList = ArrayList<ServiceModel>()

    override fun getLayoutId(): Int {
        return R.layout.fragment_services_one
    }

    override fun initializeViews() {
        getMainActivity()?.setDashboardVisibility(View.GONE)
        getMainActivity()?.setTitleVisibility(View.VISIBLE)

        val servicesImages = resources.obtainTypedArray(R.array.services_images)
        val servicesNames = resources.getStringArray(R.array.services_name)
        servicesList.clear()
        for ((index, serviceName) in servicesNames.withIndex()) {
            if (index == 0 || index == servicesNames.size - 1) {
                servicesList.add(ServiceModel(servicesImages.getResourceId(index, -1), serviceName, false))
            } else {
                servicesList.add(ServiceModel(servicesImages.getResourceId(index, -1), serviceName, true))
            }
        }

        viewManager = GridLayoutManager(getMainActivity()?.applicationContext, 3)
        viewAdapter = ServicesAdapter(servicesList, this)

        rvServices = rv_services.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

        }
    }

    override fun setListeners() {
        //no listeners
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.services)
    }

    override fun serviceIcon(): Int {
        return R.mipmap.ic_services
    }

    override fun backBtnVisibility(): Int {
        return View.GONE
    }

    override fun itemClick(position: Int, serviceModel: ServiceModel) {
        when (position) {
            0 -> {
                getMainActivity()?.addFragment(HealthServiceFragment())
            }
            12 -> {
                getMainActivity()?.addFragment(GeneralFragment())
            }
        }
    }
}