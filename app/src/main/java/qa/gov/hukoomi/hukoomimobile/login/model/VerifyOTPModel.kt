package qa.gov.hukoomi.hukoomimobile.login.model

data class VerifyOTPModel(val transactionId: String,
                          val OTP: String) {
}