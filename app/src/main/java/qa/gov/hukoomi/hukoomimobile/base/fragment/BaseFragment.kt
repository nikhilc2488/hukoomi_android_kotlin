package qa.gov.hukoomi.hukoomimobile.base.fragment

import android.support.v4.app.Fragment
import org.json.JSONException
import qa.gov.hukoomi.hukoomimobile.main.MainActivity
import qa.gov.hukoomi.hukoomimobile.login.activity.LoginActivity

open class BaseFragment: Fragment() {

    fun getMainActivity(): MainActivity {
        return activity as MainActivity
    }

    fun loadJSONFromAssets(): String {
        try {
            val inputStream = getMainActivity().assets.open("DirectoryFile.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            val json = String(buffer)
            return json
        } catch (exception: JSONException) {
            exception.printStackTrace()
            return ""
        }
    }
}