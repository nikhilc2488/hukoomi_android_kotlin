package qa.gov.hukoomi.hukoomimobile.base.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryDetailModel
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryDetailResponse
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardCheckExpiryModel
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardCheckExpiryResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.LoginModel
import qa.gov.hukoomi.hukoomimobile.login.model.LoginResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.OTPResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.VerifyOTPModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.base_url
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class WebserviceFactory {

    private val webservice: Webservice

    private var httpClient: OkHttpClient.Builder
    private var builder: Retrofit.Builder

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient = OkHttpClient.Builder().connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.MINUTES)
            .writeTimeout(5, TimeUnit.MINUTES).addInterceptor(interceptor)

        builder = Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(GsonConverterFactory.create())

        webservice = createService(Webservice::class.java)
    }

    fun <S> createService(serviceClass: Class<S>): S {

        httpClient.addInterceptor { chain ->
            val original = chain.request()

            val requestBuilder = original.newBuilder()
                .header("Content-Type", "application/json")
                .method(original.method(), original.body())

            val request = requestBuilder.build()
            chain.proceed(request)
        }

        val client = httpClient.build()
        val retrofit = builder.client(client).build()
        return retrofit.create(serviceClass)
    }

    fun login(loginModel: LoginModel): Call<OTPResponseModel> {
        return webservice.login(loginModel)
    }

    fun verifyOTP(verifyOTPModel: VerifyOTPModel): Call<LoginResponseModel> {
        return webservice.verifyOTP(verifyOTPModel)
    }

    fun checkHealthCardExpiry(checkExpiryModel: HealthCardCheckExpiryModel): Call<HealthCardCheckExpiryResponseModel> {
        return webservice.checkHealthCardExpiry(checkExpiryModel)
    }


}