package qa.gov.hukoomi.hukoomimobile.main.model

data class SideMenuModel(val sideMenuIcon: Int,
                         val sideMenuName: String) {
}