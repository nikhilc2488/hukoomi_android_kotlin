package qa.gov.hukoomi.hukoomimobile.general.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_directory.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryModel

class DirectoryAdapter(private val generalModel: ArrayList<DirectoryModel>,
                       val itemClick: ItemClick) :
    RecyclerView.Adapter<DirectoryAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var directoryLayout = view.directoryLayout
        var icon = view.directoryIcon
        var name = view.directoryName
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_directory, parent, false)

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Picasso.get().load(generalModel.get(position).directoryIcon).into(holder.icon)
        holder.name.text = generalModel.get(position).directoryName

        holder.directoryLayout.setOnClickListener {
            itemClick.item_click(position, generalModel.get(position))
        }
    }

    override fun getItemCount() = generalModel.size

    interface ItemClick {
        fun item_click(position: Int, directoryName: DirectoryModel)
    }
}