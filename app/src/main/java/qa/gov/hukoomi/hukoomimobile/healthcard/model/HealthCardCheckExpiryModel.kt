package qa.gov.hukoomi.hukoomimobile.healthcard.model

import android.os.Parcel
import android.os.Parcelable

data class HealthCardCheckExpiryModel(var QID: String = "",
                                      var expiryDate: String = ""): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(QID)
        parcel.writeString(expiryDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HealthCardCheckExpiryModel> {
        override fun createFromParcel(parcel: Parcel): HealthCardCheckExpiryModel {
            return HealthCardCheckExpiryModel(parcel)
        }

        override fun newArray(size: Int): Array<HealthCardCheckExpiryModel?> {
            return arrayOfNulls(size)
        }
    }
}