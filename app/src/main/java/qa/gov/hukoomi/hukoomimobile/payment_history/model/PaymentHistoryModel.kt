package qa.gov.hukoomi.hukoomimobile.payment_history.model

data class PaymentHistoryModel(val receiptNum: String,
                               val amount: String,
                               val trxDate: String,
                               val paymentStatus: String) {
}