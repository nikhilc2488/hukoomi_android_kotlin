package qa.gov.hukoomi.hukoomimobile.main

import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.toolbar.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.about.fragment.AboutUsFragment
import qa.gov.hukoomi.hukoomimobile.base.activity.BaseMainActivity
import qa.gov.hukoomi.hukoomimobile.dashboard.fragment.DashBoardFragment
import qa.gov.hukoomi.hukoomimobile.favorite.fragment.FavoriteFragment
import qa.gov.hukoomi.hukoomimobile.language.fragment.LanguageFragment
import qa.gov.hukoomi.hukoomimobile.login.activity.LoginActivity
import qa.gov.hukoomi.hukoomimobile.login.model.Profile
import qa.gov.hukoomi.hukoomimobile.main.adapter.SideMenuAdapter
import qa.gov.hukoomi.hukoomimobile.main.model.SideMenuModel
import qa.gov.hukoomi.hukoomimobile.profile.fragment.ProfileFragment
import qa.gov.hukoomi.hukoomimobile.services.fragment.ServicesFragment
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import qa.gov.hukoomi.hukoomimobile.utils.Constants.app_language
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import qa.gov.hukoomi.hukoomimobile.utils.get
import qa.gov.hukoomi.hukoomimobile.utils.launchActivityAddFlag
import qa.gov.hukoomi.hukoomimobile.utils.put

class MainActivity : BaseMainActivity(), BottomNavigationView.OnNavigationItemSelectedListener,
    DrawerLayout.DrawerListener, SideMenuAdapter.ItemClick {

    private lateinit var title_toolbar: Toolbar
    private lateinit var titleBackBtn: ImageView
    private lateinit var back_btn: ImageView
    private lateinit var dashboard_bg: LinearLayout
    private lateinit var title_bg: LinearLayout
    private lateinit var service_title: TextView
    private lateinit var service_icon: ImageView

    private lateinit var user_name: TextView
    private lateinit var settings_btn: ImageView
    private var profile: Profile? = null

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var sideMenu: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var bottomNavigation: BottomNavigationView

    private lateinit var progressBar: LinearLayout

    var bottomSelectedItem: Int? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun initializeViews() {
//        if (getPreference().get(app_language, "").equals(resources.getString(R.string.local_code_ar))) {
//            getPreference().put(app_language, resources.getString(R.string.local_code_ar))
//            setLocale(resources.getString(R.string.local_code_ar))
//        } else {
//            getPreference().put(app_language, resources.getString(R.string.local_code_en))
//            setLocale(resources.getString(R.string.local_code_en))
//        }

        title_toolbar = titleToolbar as Toolbar
        titleBackBtn = title_toolbar.backBtn as ImageView
        back_btn = backBtn
        dashboard_bg = dashboardBg
        title_bg = titleBg
        service_title = serviceTitle
        service_icon = serviceIcon

        user_name = userName
        val profileJSON = getPreference().get(Constants.profile, "")
        profile = Gson().fromJson<Profile>(profileJSON, Profile::class.java)
        user_name.text = "${profile?.UserFirstNameEN} ${profile?.UserLastNameEN}"

        settings_btn = settingsBtn
        settings_btn.visibility = if (getPreference().get(Constants.asGuest, false)) View.GONE else View.VISIBLE
        drawerLayout = navigation_drawer
        bottomNavigation = bottom_navigation

        viewManager = LinearLayoutManager(applicationContext)
        viewAdapter = SideMenuAdapter(sideMenuList(), this)

        sideMenu = rv_sideMenu.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        progressBar = progressBarLayout
        bottomSelectedItem = R.id.dashboard
    }

    override fun setListeners() {
        titleBackBtn.setOnClickListener { onBackPressed() }
        back_btn.setOnClickListener { onBackPressed() }

        settings_btn.setOnClickListener {
            profile?.apply {
                setDashboardVisibility(View.GONE)
                setTitleVisibility(View.VISIBLE)
                addFragment(ProfileFragment())
            }
        }

        drawerLayout.addDrawerListener(this)
        bottomNavigation.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.dashboard -> {
                bottomSelectedItem = R.id.dashboard
                setNavigation(DashBoardFragment())
                return true
            }
            R.id.services -> {
                bottomSelectedItem = R.id.services
                setNavigation(fragment = ServicesFragment())
                return true
            }
            R.id.favorite -> {
                bottomSelectedItem = R.id.favorite
                setNavigation(fragment = FavoriteFragment())
                return true
            }
            R.id.menu -> {
                drawerLayout.openDrawer(GravityCompat.START)
                return true
            }
        }
        return false
    }

    fun setNavigation(
        fragment: Fragment,
        addToBackStack: Boolean = false
    ) {
        innerPage = false
        clearBackStack()
        addFragment(fragment, addToBackStack)
    }

    override fun onDrawerStateChanged(p0: Int) {}

    override fun onDrawerClosed(p0: View) {
//        bottomNavigation.selectedItemId = bottomSelectedItem!!
        setBackBtnVisibility(View.GONE)
    }

    override fun onDrawerOpened(p0: View) {
        setBackBtnVisibility(View.VISIBLE)
    }

    override fun onDrawerSlide(drawerView: View, slideOffSet: Float) {
        var width = sideMenu.width
        val moveFactor = width * slideOffSet
        main_frameLayout.setTranslationX(moveFactor)
    }

    override fun itemClick(position: Int, sideMenuItem: SideMenuModel) {
        when (position) {
            0 -> {
                addFragment(LanguageFragment())
                drawerLayout.closeDrawers()
            }
            3 -> {
                addFragment(AboutUsFragment())
                drawerLayout.closeDrawers()
            }
            4 -> {
                getPreference().put(Constants.profile, "")
                launchActivityAddFlag<LoginActivity>()
            }
        }
    }

    fun setServiceTitle(serviceTitle: String) {
        service_title.text = serviceTitle
    }

    fun setServiceIcon(serviceIcon: Int) {
        Picasso.get().load(serviceIcon).into(service_icon)
    }

    fun setDashboardVisibility(visibility: Int) {
        dashboard_bg.visibility = visibility
    }

    fun setTitleVisibility(visibility: Int) {
        title_bg.visibility = visibility
    }

    fun setBackBtnVisibility(visibility: Int) {
        back_btn.visibility = visibility
        titleBackBtn.visibility = visibility
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            bottomNavigation.selectedItemId = bottomSelectedItem!!
            drawerLayout.closeDrawers()
        } else if (bottom_navigation.selectedItemId == R.id.dashboard || innerPage) {
            if (progressBar.visibility == View.VISIBLE) {
                hideLoading()
                return
            }
            innerPage = false
            super.onBackPressed()
        } else {
            bottom_navigation.selectedItemId = R.id.dashboard
            addFragment(DashBoardFragment(), false)
        }
    }
}