package qa.gov.hukoomi.hukoomimobile.general.model

import android.os.Parcel
import android.os.Parcelable

data class DirectoryModel(val directoryIcon: Int,
                          val directoryType: String,
                          val directoryName: String): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(directoryIcon)
        dest?.writeString(directoryType)
        dest?.writeString(directoryName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DirectoryModel> {
        override fun createFromParcel(parcel: Parcel): DirectoryModel {
            return DirectoryModel(parcel)
        }

        override fun newArray(size: Int): Array<DirectoryModel?> {
            return arrayOfNulls(size)
        }
    }

}