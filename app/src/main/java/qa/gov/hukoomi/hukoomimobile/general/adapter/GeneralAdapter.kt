package qa.gov.hukoomi.hukoomimobile.general.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_general.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.general.model.GeneralModel

class GeneralAdapter(
    val generalModel: ArrayList<GeneralModel>,
    val itemClick: ItemClick
) :
    RecyclerView.Adapter<GeneralAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var generalItemIcon = view.generalItemIcon
        var generalItemName = view.generalItemName
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_general, parent, false)

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Picasso.get().load(generalModel.get(position).generalItemIcon).into(holder.generalItemIcon)

        holder.generalItemName.text = generalModel.get(position).generalItemName

        holder.view.setOnClickListener { itemClick.itemClick(position, generalModel.get(position)) }
        holder.view.setOnLongClickListener { itemClick.itemLongClick(position, generalModel.get(position)) }
    }

    override fun getItemCount() = generalModel.size

    interface ItemClick {
        fun itemClick(position: Int, generalItem: GeneralModel)
        fun itemLongClick(position: Int, generalItem: GeneralModel): Boolean
    }
}