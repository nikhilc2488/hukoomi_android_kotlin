package qa.gov.hukoomi.hukoomimobile.healthcard.model

data class HealthCardCheckExpiryResponseModel(val errorCode: String,
                                              val status: Boolean,
                                              val data: Data,
                                              val errors: List<Errors>) {
}

data class Data(val responseStatus: String,
                val remarksEng: String,
                val remarksArb: String,
                val patientID: String,
                val holderFullNameEN: String,
                val holderFullNameAR: String,
                val maxNoOfYears: String,
                val cardExpiryDate: String,
                val nationalityFlag: String) {

}

data class Errors(val msg: String)