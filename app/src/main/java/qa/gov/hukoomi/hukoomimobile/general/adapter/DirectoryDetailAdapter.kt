package qa.gov.hukoomi.hukoomimobile.general.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_directory_detail.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryDetailModel
import qa.gov.hukoomi.hukoomimobile.general.model.GeneralModel
import android.text.style.UnderlineSpan
import android.text.SpannableString
import android.widget.TextView


class DirectoryDetailAdapter(val context: Context,
                             private val generalModel: ArrayList<DirectoryDetailModel>,
                             val itemClick: ItemClick) :
    RecyclerView.Adapter<DirectoryDetailAdapter.MyViewHolder>() {

    var currentPosition = 0

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var layout = view.layout
        var directoryName = view.directoryName
        var arrow = view.arrow

        var expandedLayout = view.expandedLayout
        var email = view.email
        var fax = view.fax
        var phone = view.phone
        var phoneLayout = view.phoneLayout
        var location = view.location

    }

    fun setSpannable(view:TextView)
    {
        val content = SpannableString(view.text)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        view.setText(content)
    }
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_directory_detail, parent, false)

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.directoryName.text = generalModel.get(position).name
        holder.email.text = generalModel.get(position).email
        holder.fax.text = generalModel.get(position).fax
        holder.phone.text = generalModel.get(position).phone
        holder.location.text = generalModel.get(position).address
        holder.expandedLayout.visibility = View.GONE
        holder.layout.setBackgroundColor(context.resources.getColor(R.color.colorAccent))
        holder.directoryName.setTextColor(context.resources.getColor(R.color.dashboard_text_color))
        Picasso.get().load(R.mipmap.ic_expand).into(holder.arrow)
        setSpannable(holder.email);
        setSpannable(holder.phone);
        setSpannable(holder.location);
        if (currentPosition == position) {
            holder.expandedLayout.visibility = View.VISIBLE
            holder.layout.setBackgroundColor(context.resources.getColor(R.color.colorPrimary))
            holder.directoryName.setTextColor(context.resources.getColor(R.color.colorAccent))
            Picasso.get().load(R.mipmap.ic_collapse).into(holder.arrow)
        }

        holder.layout.setOnClickListener {
            currentPosition = position
            notifyDataSetChanged()
        }

        holder.phoneLayout.setOnClickListener {
            itemClick.dial_number(generalModel.get(position).phone)
        }

        holder.location.setOnClickListener{
            itemClick.openMap(generalModel.get(position).address)
        }

        holder.email.setOnClickListener {
            itemClick.openEmail(generalModel.get(position).email)
        }
    }

    override fun getItemCount() = generalModel.size

    interface ItemClick {
        fun dial_number(phone: String)
        fun openMap(address: String)
        fun openEmail(email: String)
    }
}