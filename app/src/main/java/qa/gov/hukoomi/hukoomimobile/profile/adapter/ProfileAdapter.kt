package qa.gov.hukoomi.hukoomimobile.profile.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.row_profile.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.profile.model.ProfileModel

class ProfileAdapter(private val profileModel: ArrayList<ProfileModel>) :
    RecyclerView.Adapter<ProfileAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var key = view.key
        var value = view.value
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_profile, parent, false)

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.key.text = profileModel.get(position).key
        holder.value.text = profileModel.get(position).value
    }

    override fun getItemCount() = profileModel.size

}