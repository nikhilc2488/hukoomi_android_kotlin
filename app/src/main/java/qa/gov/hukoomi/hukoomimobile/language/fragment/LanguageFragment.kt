package qa.gov.hukoomi.hukoomimobile.language.fragment


/**
 * Created by Nikhil Chindarkar on 28-01-2019.
 * Malomatia India Pvt. Ltd
 */
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import kotlinx.android.synthetic.main.fragment_about_us.*
import kotlinx.android.synthetic.main.fragment_language.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.about.adapter.AboutUsAdapter
import qa.gov.hukoomi.hukoomimobile.about.model.AboutUsModel
import qa.gov.hukoomi.hukoomimobile.base.activity.BaseLoginActivity
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment
import qa.gov.hukoomi.hukoomimobile.login.activity.LoginActivity
import qa.gov.hukoomi.hukoomimobile.main.MainActivity
import qa.gov.hukoomi.hukoomimobile.utils.*

class LanguageFragment: BaseMainFragment() , AdapterView.OnItemSelectedListener{

    private lateinit var spinner: Spinner

    override fun getLayoutId(): Int {
        return R.layout.fragment_language
    }

    override fun initializeViews() {

        spinner = languagespinner;
        spinner.setOnItemSelectedListener(this)

        getMainActivity()?.setDashboardVisibility(View.GONE)
        getMainActivity()?.setTitleVisibility(View.VISIBLE)



    }

    override fun setListeners() {
        //no listeners
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.language)
    }

    override fun serviceIcon(): Int {
        return R.mipmap.language
    }

    override fun backBtnVisibility(): Int {
        return View.VISIBLE
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {


            if(position==0)
            {
                getPreference(this!!.getMainActivity()!!).put(Constants.app_language, resources.getString(R.string.local_code_en))
                setLocale(resources.getString(R.string.local_code_en),this!!.getMainActivity()!!)
                getLoginActivity()?.launchActivity<MainActivity>()

            }
            else {
                getPreference(this!!.getMainActivity()!!).put(Constants.app_language, resources.getString(R.string.local_code_ar))
                setLocale(resources.getString(R.string.local_code_ar),this!!.getMainActivity()!!)
                getLoginActivity()?.launchActivity<MainActivity>()
            }

    }

    fun getLoginActivity(): LoginActivity? {
        return activity as? LoginActivity
    }
}