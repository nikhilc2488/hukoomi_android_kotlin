package qa.gov.hukoomi.hukoomimobile.general.fragment

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_directory_detail.*
import org.json.JSONObject
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.base.helpers.PermissionUtils
import qa.gov.hukoomi.hukoomimobile.general.adapter.DirectoryDetailAdapter
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import qa.gov.hukoomi.hukoomimobile.utils.Constants.directory_key
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import qa.gov.hukoomi.hukoomimobile.utils.inflate
import android.content.Intent
import android.net.Uri
import android.telecom.Call
import android.util.Log
import com.android.volley.*
import com.android.volley.VolleyLog.TAG
import com.android.volley.toolbox.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.support.v4.uiThread
import org.json.JSONArray
import qa.gov.hukoomi.hukoomimobile.base.network.WebserviceFactory
import qa.gov.hukoomi.hukoomimobile.general.APIController
import qa.gov.hukoomi.hukoomimobile.general.BackendVolley
import qa.gov.hukoomi.hukoomimobile.general.ServiceInterface
import qa.gov.hukoomi.hukoomimobile.general.ServiceVolley
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryDetailModel
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryDetailResponse
import qa.gov.hukoomi.hukoomimobile.login.fragment.OTPFragment
import qa.gov.hukoomi.hukoomimobile.login.model.OTPResponseModel
import qa.gov.hukoomi.hukoomimobile.utils.toast
import java.net.URL
import javax.security.auth.callback.Callback


class DirectoryDetailFragment: BaseFragment(), DirectoryDetailAdapter.ItemClick {
    private lateinit var directory_list: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var directory: DirectoryModel



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_directory_detail)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        innerPage = true
        getMainActivity().setServiceIcon(R.mipmap.ic_directory_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        directory = arguments?.getParcelable(directory_key) as DirectoryModel
        getMainActivity().setServiceTitle(directory.directoryName)
        //val list = parseJSON(loadJSONFromAssets(), directory.directoryType)

/*
        val cache = DiskBasedCache(context?.cacheDir, 1024 * 1024) // 1MB cap

// Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())



// Instantiate the RequestQueue with the cache and network. Start the queue.
        val requestQueue = RequestQueue(cache, network).apply {
            start()
        }
        // Formulate the request and handle the response.
        val stringRequest = StringRequest(
            Request.Method.GET, "https://sportal.www.gov.qa/GoogleAssistService/ga/gaService/directoryList",
            Response.Listener<String> { response ->
                // Do something with the response

                val jsonArray = JSONArray()
                jsonArray.put(response)

                Log.d("JSON",jsonArray.toString());
            },
            Response.ErrorListener { error ->
                // Handle error

            })

        requestQueue.add(stringRequest)*/
        val mProgressDialog = indeterminateProgressDialog("Loading...", "")

        doAsync{
            mProgressDialog.show()
            val result = URL("https://sportal.www.gov.qa/GoogleAssistService/ga/gaService/directoryList").readText()
            uiThread {
               loadDirectory(result)
                mProgressDialog.dismiss()
            }
        }
     /*   val service = ServiceVolley()
        val apiController = APIController(service)
        val path = "gaService/directoryList"
        val params = JSONObject();
        apiController.post(path, params) { response ->
            // Parse the result
            val jsonArray = JSONArray()
            jsonArray.put(response)

            Log.d("JSON",jsonArray.toString());

        }*/



    }

    fun loadDirectory(result:String)
    {
        var list = parseJSON(result, directory.directoryType)
        viewAdapter = DirectoryDetailAdapter(getMainActivity().applicationContext, list, this)

        viewManager = LinearLayoutManager(getMainActivity().applicationContext)



        directory_list = rv_directoryList.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    fun parseJSON(json: String, directoryType: String?): ArrayList<DirectoryDetailModel> {

       /* val jsonObject = JSONObject(json)
        val jsonArray = jsonObject.getJSONArray("directory")*/

        val jsonArray = JSONArray(json)
        var list: ArrayList<DirectoryDetailModel> = ArrayList(jsonArray.length())

        for (directoryIndex in 0 until jsonArray.length()) {
            val directory = jsonArray.getJSONObject(directoryIndex)
            if (directoryType.equals(directory.getString("directoryType"), ignoreCase = true))
                list.add(DirectoryDetailModel(directory.getString("directoryType"),
                    directory.getString("name"),
                    directory.getString("address"),
                    directory.getString("email"),
                    directory.getString("phone"),
                    directory.getString("fax"),
                    directory.getString("website"),
                    directory.getString("image")))
        }

        list.sortedWith(compareBy({ it.directoryType }))
        return list
    }

    override fun dial_number(phone: String) {
        if (PermissionUtils().requestPhoneCallPermission(getMainActivity())) {
            val phoneIntent = Intent(
                Intent.ACTION_DIAL, Uri.fromParts(
                    "tel", phone, null
                )
            )
            startActivity(phoneIntent)
        }
    }

    override fun openMap(address: String) {
        val map = "http://maps.google.co.in/maps?q=$address"
        val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
        startActivity(i)
    }

    override fun openEmail(email: String) {
        val to = email

        val intent = Intent(Intent.ACTION_SEND)
        val addressees = arrayOf(to)
        intent.putExtra(Intent.EXTRA_EMAIL, addressees)
        intent.setType("message/rfc822")
        startActivity(Intent.createChooser(intent, "Send Email using:"));
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Constants.requestCode_phoneCall_permission -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        getMainActivity().applicationContext,
                        "Permission Granted! You can call.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}