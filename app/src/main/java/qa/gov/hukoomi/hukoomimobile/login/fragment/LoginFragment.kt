package qa.gov.hukoomi.hukoomimobile.login.fragment

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.widget.*
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_welcome.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseLoginFragment
import qa.gov.hukoomi.hukoomimobile.base.helpers.UIHelper
import qa.gov.hukoomi.hukoomimobile.base.network.WebserviceFactory
import qa.gov.hukoomi.hukoomimobile.login.model.LoginModel
import qa.gov.hukoomi.hukoomimobile.login.model.OTPResponseModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import qa.gov.hukoomi.hukoomimobile.utils.Constants.debug
import qa.gov.hukoomi.hukoomimobile.utils.Constants.transactionID
import qa.gov.hukoomi.hukoomimobile.utils.get
import qa.gov.hukoomi.hukoomimobile.utils.put
import qa.gov.hukoomi.hukoomimobile.utils.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginFragment : BaseLoginFragment() {

    private lateinit var tvLanguageSelector: TextView
    private lateinit var back_btn: ImageView
    private lateinit var etUsername: EditText
    private lateinit var etPassword: EditText
    private lateinit var login_btn: Button
    private lateinit var forgotPassword: TextView
    private lateinit var rememberMe: CheckBox
    private var rememberQID: Boolean = false

    private lateinit var webView_toolbar: Toolbar
    private lateinit var webViewBackBtn: ImageView
    private lateinit var register_now: LinearLayout

    override fun getLayoutId(): Int {
        return R.layout.fragment_login
    }

    override fun initializeViews() {
        tvLanguageSelector = tv_language
        back_btn = backBtn
        etUsername = et_username
        etUsername.setText(getLoginActivity()?.getPreference()?.get(Constants.rememberQID, ""))
        etPassword = et_password
        login_btn = loginButtn
        forgotPassword = tv_forgotPassword
        rememberMe = rememberMe_check
        rememberMe.isChecked = getLoginActivity()?.getPreference()!!.get(Constants.rememberMeCheck, false)
        rememberQID = getLoginActivity()?.getPreference()!!.get(Constants.rememberMeCheck, false)
        register_now = registerNow

        webView_toolbar = webViewTlbar
        webViewBackBtn = webView_backBttn
    }

    override fun setListeners() {
        tvLanguageSelector.setOnClickListener {
            if (getLoginActivity()?.getPreference()!!.get(Constants.app_language, "").equals(resources.getString(R.string.local_code_ar))) {
                getLoginActivity()?.getPreference()!!.put(Constants.app_language, resources.getString(R.string.local_code_en))
                setLocale(resources.getString(R.string.local_code_en))
            } else {
                getLoginActivity()?.getPreference()!!.put(Constants.app_language, resources.getString(R.string.local_code_ar))
                setLocale(resources.getString(R.string.local_code_ar))
            }
        }
        webViewBackBtn.setOnClickListener { getLoginActivity()?.onBackPressed() }
        back_btn.setOnClickListener { getLoginActivity()?.onBackPressed() }
        login_btn.setOnClickListener {
            loginBtnAction()
        }
        forgotPassword.setOnClickListener {
            openWebPage(loginWebViewLayout, login_webView, weburl, resources.getString(R.string.forgot_password_link))
        }
        register_now.setOnClickListener {
            openWebPage(
                loginWebViewLayout,
                login_webView,
                    weburl,
                resources.getString(R.string.hukoomi_registration_link)
            )
        }
        rememberMe.setOnCheckedChangeListener { buttonView, isChecked ->
            rememberQID = isChecked
        }
    }

    fun loginBtnAction() {
        if (etUsername.length() < 11) {
            etUsername.error = resources.getString(R.string.qid_must_be_number)
            return
        }
        if (TextUtils.isEmpty(etUsername.text) || TextUtils.isEmpty(etPassword.text)) {
            getLoginActivity()?.toast(resources.getString(R.string.username_password_missing))
            return
        }
        if (rememberQID) {
            setRememberMePreference(etUsername.text.toString())
        } else {
            setRememberMePreference("")
        }
        if (UIHelper().isNetworkAvailable(getLoginActivity()?.applicationContext)) {
            getLoginActivity()?.showLoading()
            if (debug) {
                getLoginActivity()?.hideLoading()
                getLoginActivity()?.addFragment(OTPFragment(), addToBackStack = true)
            } else {
                loginService(LoginModel(etUsername.text.toString(), etPassword.text.toString()))
            }
        } else {
            getLoginActivity()?.hideLoading()
            getLoginActivity()?.toast(resources.getString(R.string.check_connection))
        }
    }

    fun setRememberMePreference(username: String) {
        getLoginActivity()?.getPreference()?.put(Constants.rememberQID, username)
        getLoginActivity()?.getPreference()?.put(Constants.rememberMeCheck, rememberQID)
    }

    fun loginService(loginModel: LoginModel) {
        val webService = WebserviceFactory()
        val callResponse = webService.login(loginModel)

        callResponse.enqueue(object : Callback<OTPResponseModel> {
            override fun onFailure(call: Call<OTPResponseModel>, t: Throwable) {
                getLoginActivity()?.hideLoading()
                getLoginActivity()?.toast(t.message)
            }

            override fun onResponse(call: Call<OTPResponseModel>, response: Response<OTPResponseModel>) {
                getLoginActivity()?.hideLoading()
                if (response.isSuccessful) {
                    if (response.body()?.errorCode == "-1") {
                        Log.e("transactionID", response.body()?.OTP_transactionID)
                        val otpFragment = OTPFragment()
                        val bundle = Bundle()
                        bundle.putString(transactionID, response.body()?.OTP_transactionID)
                        otpFragment.arguments = bundle
                        getLoginActivity()?.addFragment(otpFragment, addToBackStack = true)
                    } else {
                        getLoginActivity()?.toast(response.body()?.description)
                    }
                }
            }
        })
    }
}