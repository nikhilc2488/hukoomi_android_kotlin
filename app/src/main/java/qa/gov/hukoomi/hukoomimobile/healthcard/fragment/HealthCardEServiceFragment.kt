package qa.gov.hukoomi.hukoomimobile.healthcard.fragment

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.fragment_health.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment
import qa.gov.hukoomi.hukoomimobile.healthcard.adapter.HealthAdapter
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardServiceModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage

class HealthCardEServiceFragment: BaseMainFragment(), HealthAdapter.ItemClick {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun getLayoutId(): Int {
        return R.layout.fragment_health
    }

    override fun initializeViews() {
        innerPage = true

        val listOfServices = ArrayList<HealthCardServiceModel>()
        listOfServices.add(HealthCardServiceModel(R.mipmap.ic_expired_items, resources.getString(R.string.health_card_check_expiry)))
        listOfServices.add(HealthCardServiceModel(R.mipmap.ic_health_card_renew, resources.getString(R.string.health_card_renew)))
        listOfServices.add(HealthCardServiceModel(R.mipmap.ic_health_card_reprint, resources.getString(R.string.health_card_reprint)))

        viewManager = LinearLayoutManager(getMainActivity()?.applicationContext)
        viewAdapter = HealthAdapter(listOfServices, this)

        recyclerView = rv_healthServices.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

        }
    }

    override fun setListeners() {
        //no listeners
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.health_card_e_services)
    }

    override fun serviceIcon(): Int {
        return R.mipmap.ic_health_card_service
    }

    override fun backBtnVisibility(): Int {
        return View.VISIBLE
    }

    override fun itemClick(position: Int, healthCardModel: HealthCardServiceModel) {
        when(position) {
            0 -> {
                getMainActivity()?.addFragment(HealthCardCheckExpiryFragment())
            }
            1 -> {
                getMainActivity()?.addFragment(HealthCardRenewFragmentOne())
            }
            2 -> {
                getMainActivity()?.addFragment(HealthCardReprintFragmentOne())
            }
        }
    }

    override fun itemLongClick(position: Int, healthCardModel: HealthCardServiceModel): Boolean {
        return false
    }
}