package qa.gov.hukoomi.hukoomimobile.login.model

data class LoginResponseModel(val errorCode: String,
                              val profile: Profile,
                              val ss: String,
                              val error: String) {
}

data class Profile(var errorCode: String = "",
                   var AccountLastProfileSaveDate: String = "",
                   var AccountLevel: String = "",
                   var AccountType: String = "",
                   var StatusPhoneNumberVerification: String = "",
                   var UserAddressBuildingNumber: String = "",
                   var UserAddressPostOfficeBox: String = "",
                   var UserAddressStreetName: String = "",
                   var UserAddressZone: String = "",
                   var UserBirthdate: String = "",
                   var UserEmail: String = "",
                   var UserFirstNameAR: String = "",
                   var UserFirstNameEN: String = "",
                   var UserGender: String = "",
                   var UserLandLine: String = "",
                   var UserLastNameAR: String = "",
                   var UserLastNameEN: String = "",
                   var UserMiddleNameAR: String = "",
                   var UserMiddleNameEN: String = "",
                   var UserMobileNumber: String = "",
                   var UserNationality: String = "",
                   var UserQID: String = "",
                   var cn: String = "",
                   var UserEID: String = "") {}