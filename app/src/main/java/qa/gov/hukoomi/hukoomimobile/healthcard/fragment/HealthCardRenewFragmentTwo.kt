package qa.gov.hukoomi.hukoomimobile.healthcard.fragment

import android.os.Bundle
import android.support.v7.widget.CardView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fragment_health_card_renew_two.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardCheckExpiryModel
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardRenewModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.checkExpiryModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.healthCardRenewalModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import qa.gov.hukoomi.hukoomimobile.utils.Constants.qid_number
import qa.gov.hukoomi.hukoomimobile.utils.inflate

class HealthCardRenewFragmentTwo : BaseFragment(), View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private lateinit var qidNumber: TextView
    private lateinit var expiryDate: TextView
    private lateinit var num_of_years: TextView
    private var yearsCounter: Int = 1
    private lateinit var inc_years: ImageView
    private lateinit var dec_years: ImageView
    private lateinit var etPhoneNumber: EditText
    private lateinit var byEmail_group: RadioGroup
    private lateinit var yesByEmail: RadioButton
    private lateinit var noByEmail: RadioButton
    private lateinit var notification_group: RadioGroup
    private lateinit var yesSMSNotification: RadioButton
    private lateinit var noSMSNotification: RadioButton
    private lateinit var previous_btn: Button
    private lateinit var next_btn: Button

    private lateinit var healthCardCheckExpiryModel: HealthCardCheckExpiryModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_health_card_renew_two)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val bundle = arguments

        innerPage = true
        getMainActivity().setServiceTitle(resources.getString(R.string.health_card_renew))
        getMainActivity().setServiceIcon(R.mipmap.ic_health_card_renew_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        initializeViews()
        healthCardCheckExpiryModel = bundle?.getParcelable(checkExpiryModel) as HealthCardCheckExpiryModel
        qidNumber.text = healthCardCheckExpiryModel.QID
        expiryDate.text = healthCardCheckExpiryModel.expiryDate
    }

    fun initializeViews() {
        qidNumber = qidNumber_stepTwo
        expiryDate = expiryDate_stepTwo
        num_of_years = numberOfYearsCounter
        inc_years = increaseYears
        dec_years = decreseYears
        etPhoneNumber = et_phoneNumber
        byEmail_group = receiptByEmail_group
        yesByEmail = yes_byEmail
        noByEmail = no_byEmail
        notification_group = smsNotification_group
        yesSMSNotification = yes_smsNotification
        noSMSNotification = no_smsNotification
        previous_btn = previousBtn_stepTwo
        next_btn = nextBtn

        setListeners()
    }

    fun setListeners() {
        inc_years.setOnClickListener(this)
        dec_years.setOnClickListener(this)
        byEmail_group.setOnCheckedChangeListener(this)
        notification_group.setOnCheckedChangeListener(this)
        previous_btn.setOnClickListener(this)
        next_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            inc_years -> {
                if (yearsCounter == 5) {
                    return
                }
                yearsCounter++
                num_of_years.text = yearsCounter.toString()
            }
            dec_years -> {
                if (yearsCounter == 1) {
                    return
                }
                yearsCounter--
                num_of_years.text = yearsCounter.toString()
            }
            previous_btn -> {
                getMainActivity().onBackPressed()
            }
            next_btn -> {
                if (TextUtils.isEmpty(etPhoneNumber.text)) {
                    etPhoneNumber.error = resources.getString(R.string.phone_number)
                    return
                }
                if (byEmail_group.checkedRadioButtonId == -1) {
                    Toast.makeText(
                        getMainActivity().applicationContext,
                        "${resources.getString(R.string.receive_receipts_by_email)}",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
                if (notification_group.checkedRadioButtonId == -1) {
                    Toast.makeText(
                        getMainActivity().applicationContext,
                        "${resources.getString(R.string.receive_sms_notification)}",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }

                gotToStepThree()
            }
        }
    }

    fun gotToStepThree() {
        val healthCardRenewModel = HealthCardRenewModel(qidNumber.text.toString(),
            expiryDate.text.toString(), yearsCounter, etPhoneNumber.text.toString(), byEmail_group.checkedRadioButtonId,
            notification_group.checkedRadioButtonId)

        val bundle = Bundle()
        bundle.putParcelable(healthCardRenewalModel, healthCardRenewModel)
        val healthCardRenewThree = HealthCardRenewFragmentThree()
        healthCardRenewThree.arguments = bundle
        getMainActivity().addFragment(healthCardRenewThree)
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.yes_byEmail -> {

            }
            R.id.no_byEmail -> {

            }
            R.id.yes_smsNotification -> {

            }
            R.id.no_smsNotification -> {

            }
        }
    }
}