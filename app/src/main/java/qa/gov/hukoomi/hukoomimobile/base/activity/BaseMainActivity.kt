package qa.gov.hukoomi.hukoomimobile.base.activity

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.`interface`.LoadingListeners
import qa.gov.hukoomi.hukoomimobile.dashboard.fragment.DashBoardFragment
import qa.gov.hukoomi.hukoomimobile.main.model.SideMenuModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import java.util.*

abstract class BaseMainActivity: AppCompatActivity(), LoadingListeners {

    private lateinit var sharedPreferences: SharedPreferences

    fun getPreference(): SharedPreferences {
        sharedPreferences = getSharedPreferences(Constants.pref_name, Context.MODE_PRIVATE)
        return sharedPreferences
    }

    abstract fun getLayoutId(): Int
    abstract fun initializeViews()
    abstract fun setListeners()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())

        initializeViews()
        setListeners()

        addFragment(DashBoardFragment(), addToBackStack = false)
    }

    fun addFragment(fragment: Fragment, addToBackStack: Boolean = true, cleanBackStack: Boolean = false,
                    frameID: Int = R.id.main_frame_container) {
        if (cleanBackStack) { clearBackStack() }
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(frameID, fragment)
        if (addToBackStack) { fragmentTransaction.addToBackStack(null) }
        fragmentTransaction.commit()
    }

    fun clearBackStack() {
        val fragmentManager = supportFragmentManager
        if (fragmentManager.backStackEntryCount > 0) {
            val first = fragmentManager.getBackStackEntryAt(0)
            fragmentManager.popBackStack(first.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    fun sideMenuList(): ArrayList<SideMenuModel> {
        val itemList = arrayListOf<SideMenuModel>()
        itemList.add(SideMenuModel(R.mipmap.language, resources.getString(R.string.language)))
        itemList.add(SideMenuModel(R.mipmap.contactus, resources.getString(R.string.contact_us)))
        itemList.add(SideMenuModel(R.mipmap.termsconditions, resources.getString(R.string.term_and_conditions)))
        itemList.add(SideMenuModel(R.mipmap.ic_about_us, resources.getString(R.string.about_us)))
        itemList.add(SideMenuModel(R.mipmap.ic_logout, resources.getString(R.string.log_out)))

        return itemList
    }

    fun setLocale(lang: String) {
        val myLocale = Locale(lang)
        val res = resources
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, null)
    }
}