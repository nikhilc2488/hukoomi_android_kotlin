package qa.gov.hukoomi.hukoomimobile.services.model

data class ServiceModel(val imageId: Int,
                        val serviceName: String,
                        val disableOption: Boolean) {
}