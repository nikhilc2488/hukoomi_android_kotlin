package qa.gov.hukoomi.hukoomimobile.profile.model

data class ProfileModel(val key: String,
                        val value: String) {
}