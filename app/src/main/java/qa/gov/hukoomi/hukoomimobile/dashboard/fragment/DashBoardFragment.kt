package qa.gov.hukoomi.hukoomimobile.dashboard.fragment

import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.azan.TimeCalculator
import com.azan.types.AngleCalculationType
import com.azan.types.PrayersType
import kotlinx.android.synthetic.main.fragment_dashboard.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment
import qa.gov.hukoomi.hukoomimobile.dashboard.model.PrayerModel
import qa.gov.hukoomi.hukoomimobile.payment_history.fragment.PaymentHistoryFragment
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import qa.gov.hukoomi.hukoomimobile.utils.get
import qa.gov.hukoomi.hukoomimobile.utils.getPreference
import qa.gov.hukoomi.hukoomimobile.utils.setLocale
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DashBoardFragment : BaseMainFragment() {

    private lateinit var prayerTime_remaining: TextView
    private lateinit var prayer_name: TextView
    private lateinit var prayer_time: TextView

    private lateinit var expiringSoonDisable: ImageView
    private lateinit var expireditemDisable: ImageView
    private lateinit var paymentHistoryDisable: ImageView
    private lateinit var duePaymentDisable: ImageView
    private lateinit var appointmentDisable: ImageView

    private lateinit var paymentHistory: RelativeLayout

    override fun getLayoutId(): Int {
        return R.layout.fragment_dashboard
    }

    override fun initializeViews() {
        setLocale(getPreference(this!!.context!!).get(Constants.app_language,resources.getString(R.string.local_code_ar)), this!!.context!!)
        getMainActivity()?.setDashboardVisibility(View.VISIBLE)
        getMainActivity()?.setTitleVisibility(View.GONE)
        prayerTime_remaining = prayerTimeRemaining
        prayer_name = prayerName
        prayer_time = prayerTime

        expiringSoonDisable = expiringSoon_disable
        expiringSoonDisable.visibility = if (getMainActivity()?.getPreference()!!.get(Constants.asGuest, false)) View.VISIBLE else View.GONE
        expireditemDisable = expiredItem_disable
        expireditemDisable.visibility = if (getMainActivity()?.getPreference()!!.get(Constants.asGuest, false)) View.VISIBLE else View.GONE
        paymentHistoryDisable = paymentHistory_disable
        paymentHistoryDisable.visibility = if (getMainActivity()?.getPreference()!!.get(Constants.asGuest, false)) View.VISIBLE else View.GONE
        duePaymentDisable = duePayment_disable
        duePaymentDisable.visibility = if (getMainActivity()?.getPreference()!!.get(Constants.asGuest, false)) View.VISIBLE else View.GONE
        appointmentDisable = appointment_disable
        appointmentDisable.visibility = if (getMainActivity()?.getPreference()!!.get(Constants.asGuest, false)) View.VISIBLE else View.GONE

        paymentHistory = paymentHistoryTab

        prayerTime_remaining.text = namazTime()?.remainingTime
        prayer_name.text = namazTime()?.nextPrayerName

        val prayerTime = namazTime()?.nextPrayer
        prayer_time.text = SimpleDateFormat("hh:mm a").format(prayerTime)
    }

    override fun setListeners() {
        if (!getMainActivity()?.getPreference()!!.get(Constants.asGuest, false)) {
            paymentHistory.setOnClickListener {
                getMainActivity()?.addFragment(PaymentHistoryFragment())
            }
        }
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.my_dashboard)
    }

    override fun serviceIcon(): Int {
        return R.drawable.ic_my_dashboard
    }

    override fun backBtnVisibility(): Int {
        return View.GONE
    }

    fun namazTime(): PrayerModel? {
        val date = GregorianCalendar()

        val prayerTimes =
            TimeCalculator().date(date).location(25.27932, 51.52245, 13.0, 0.0)
                .timeCalculationMethod(AngleCalculationType.EGYPT)
                .calculateTimes()

        prayerTimes.isUseSecond = true

        val nextDayDate = GregorianCalendar()
        nextDayDate.add(Calendar.DATE, 1)
        val nextDay_prayerTimes =
            TimeCalculator().date(nextDayDate).location(25.27932, 51.52245, 13.0, 0.0)
                .timeCalculationMethod(AngleCalculationType.EGYPT)
                .calculateTimes()

        nextDay_prayerTimes.isUseSecond = true

        Log.e("currentTime", "${getCurrentTime()}")

        val prayerList = ArrayList<PrayerModel>()
        prayerList.add(PrayerModel("", prayerTimes.getPrayTime(PrayersType.FAJR), resources.getString(R.string.fajr)))
        prayerList.add(PrayerModel("", prayerTimes.getPrayTime(PrayersType.SUNRISE), resources.getString(R.string.sunrise)))
        prayerList.add(PrayerModel("", prayerTimes.getPrayTime(PrayersType.ZUHR), resources.getString(R.string.zuhr)))
        prayerList.add(PrayerModel("", prayerTimes.getPrayTime(PrayersType.ASR), resources.getString(R.string.asr)))
        prayerList.add(PrayerModel("", prayerTimes.getPrayTime(PrayersType.MAGHRIB), resources.getString(R.string.maghrib)))
        prayerList.add(PrayerModel("", prayerTimes.getPrayTime(PrayersType.ISHA), resources.getString(R.string.isha)))
        prayerList.add(PrayerModel("", nextDay_prayerTimes.getPrayTime(PrayersType.FAJR), resources.getString(R.string.fajr)))

        for ((index, prayerTime) in prayerList.withIndex()) {
            Log.e("prayerTime", prayerTime.toString())
            Log.e("difference", prayerTime.nextPrayer.compareTo(getCurrentTime()).toString())
            if (prayerTime.nextPrayer.compareTo(getCurrentTime()) > 0) {
                Log.e("i am here", prayerList.get(index).toString())

                val d1 = getCurrentTime()
                val d2 = prayerTime.nextPrayer
                val diff = d2.getTime() - d1.getTime()

                val diffSeconds = diff / 1000 % 60
                val diffMinutes = diff / (60 * 1000) % 60
                val diffHours = diff / (60 * 60 * 1000) % 24

                return PrayerModel(
                    "${String.format("%02d", diffHours)}:${String.format("%02d", diffMinutes)}:${String.format(
                        "%02d",
                        diffSeconds
                    )}",
                    prayerTime.nextPrayer,
                    prayerTime.nextPrayerName
                )
                break
            } else {
                Log.e("next here", prayerList.get(index).toString())
            }
        }

        return null
    }

    fun getCurrentTime(): Date {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val currentDate = simpleDateFormat.format(Date())
        return simpleDateFormat.parse(currentDate)
    }
}