package qa.gov.hukoomi.hukoomimobile.payment_history.fragment

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Spinner
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_payment_history.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment
import qa.gov.hukoomi.hukoomimobile.payment_history.adapter.PaymentHistoryAdapter
import qa.gov.hukoomi.hukoomimobile.payment_history.adapter.SpinnerAdapter
import qa.gov.hukoomi.hukoomimobile.payment_history.model.PaymentHistoryModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import java.util.*
import kotlin.collections.ArrayList

class PaymentHistoryFragment : BaseMainFragment() {

    private lateinit var startDate: TextView
    private lateinit var endDate: TextView

    private lateinit var spinnerServiceName: AutoCompleteTextView
    private val serviceNameList = ArrayList<String>()
    private lateinit var spinnerServiceNameAdapter: SpinnerAdapter

    private lateinit var spinnerStatus: AutoCompleteTextView
    private val statusList = ArrayList<String>()
    private lateinit var spinnerStatusAdapter: SpinnerAdapter

    private lateinit var rvPaymentHistory: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun getLayoutId(): Int {
        return R.layout.fragment_payment_history
    }

    override fun initializeViews() {
        innerPage = true
        getMainActivity()?.setDashboardVisibility(View.GONE)
        getMainActivity()?.setTitleVisibility(View.VISIBLE)

        startDate = tv_startDate
        endDate = tv_endDate

        spinnerServiceName = spinner_serviceName
        serviceNameList.add(resources.getString(R.string.health))

        spinnerServiceNameAdapter = SpinnerAdapter(getMainActivity()?.applicationContext!!, serviceNameList, "")
        //spinnerServiceName.adapter = spinnerServiceNameAdapter


        // Initialize a new array adapter object
        val adapterAutoCompleteTextView = ArrayAdapter<String>(
            this.activity, // Context
            android.R.layout.simple_dropdown_item_1line, // Layout
            serviceNameList // Array
        )


        // Set the AutoCompleteTextView adapter
        spinnerServiceName.setAdapter(adapterAutoCompleteTextView)


        // Auto complete threshold
        // The minimum number of characters to type to show the drop down
        spinnerServiceName.threshold = 1

        spinnerStatus = spinner_status
        statusList.add(resources.getString(R.string.approved))
        statusList.add(resources.getString(R.string.in_process))
        statusList.add(resources.getString(R.string.rejected))
        spinnerStatusAdapter = SpinnerAdapter(getMainActivity()?.applicationContext!!, statusList, "")
        //spinnerStatus.adapter = spinnerStatusAdapter


        // Initialize a new array adapter object
        val adapterStatus = ArrayAdapter<String>(
            this.activity, // Context
            android.R.layout.simple_dropdown_item_1line, // Layout
            statusList // Array
        )


        // Set the AutoCompleteTextView adapter
        spinnerStatus.setAdapter(adapterStatus)


        // Auto complete threshold
        // The minimum number of characters to type to show the drop down
        spinnerStatus.threshold = 1


        val paymentHistoryList = ArrayList<PaymentHistoryModel>()
        paymentHistoryList.add(PaymentHistoryModel("0001", "100", "25-12-2018", getString(R.string.in_process)))
        paymentHistoryList.add(PaymentHistoryModel("0002", "100", "25-12-2018", getString(R.string.in_process)))
        paymentHistoryList.add(PaymentHistoryModel("0003", "100", "25-12-2018", getString(R.string.in_process)))
        paymentHistoryList.add(PaymentHistoryModel("0004", "100", "25-12-2018", getString(R.string.in_process)))
        paymentHistoryList.add(PaymentHistoryModel("0005", "100", "25-12-2018", getString(R.string.in_process)))
        paymentHistoryList.add(PaymentHistoryModel("0006", "100", "25-12-2018", getString(R.string.in_process)))

        viewManager = LinearLayoutManager(getMainActivity()?.applicationContext)
        viewAdapter = PaymentHistoryAdapter(getMainActivity()?.applicationContext!!, paymentHistoryList)

        rvPaymentHistory = rv_paymentHistory.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun setListeners() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val startMonth = c.get(Calendar.MONTH) + 1
        val day = c.get(Calendar.DAY_OF_MONTH)
        startDate.text = "1/${startMonth}/${year}"
        endDate.text = "${day}/${startMonth}/${year}"

        startDate.setOnClickListener {
            val dpd = DatePickerDialog(
                getMainActivity(),
                AlertDialog.THEME_HOLO_LIGHT,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    startDate.text = "${dayOfMonth}/${(monthOfYear + 1)}/${year}"
                },
                year,
                startMonth,
                1
            )
            dpd.show()
        }
        endDate.setOnClickListener {
            val dpd = DatePickerDialog(
                getMainActivity(),
                AlertDialog.THEME_HOLO_LIGHT,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    endDate.text = "${dayOfMonth}/${(monthOfYear + 1)}/${year}"
                },
                year,
                startMonth,
                day
            )
            dpd.show()
        }
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.payment_history)
    }

    override fun serviceIcon(): Int {
        return R.mipmap.ic_payment_history_title
    }

    override fun backBtnVisibility(): Int {
        return View.VISIBLE
    }
}