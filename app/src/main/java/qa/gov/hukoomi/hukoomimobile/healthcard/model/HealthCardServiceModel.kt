package qa.gov.hukoomi.hukoomimobile.healthcard.model

data class HealthCardServiceModel(val healthIcon: Int, val healthServiceText: String) {
}