package qa.gov.hukoomi.hukoomimobile.general.model

/**
 * Created by Nikhil Chindarkar on 08-01-2019.
 * Malomatia India Pvt. Ltd
 */
data class DirectoryDetailResponse(val errorCode: String,
                                   val description: String) {
}