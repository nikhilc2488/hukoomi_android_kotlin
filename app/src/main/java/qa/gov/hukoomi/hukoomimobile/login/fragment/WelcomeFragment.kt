package qa.gov.hukoomi.hukoomimobile.login.fragment

import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_welcome.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseLoginFragment
import qa.gov.hukoomi.hukoomimobile.login.model.Profile
import qa.gov.hukoomi.hukoomimobile.main.MainActivity
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import qa.gov.hukoomi.hukoomimobile.utils.Constants.app_language
import qa.gov.hukoomi.hukoomimobile.utils.get
import qa.gov.hukoomi.hukoomimobile.utils.launchActivity
import qa.gov.hukoomi.hukoomimobile.utils.put

class WelcomeFragment : BaseLoginFragment() {

    private lateinit var tvLanguageSelector: TextView

    private lateinit var login_btn: Button
    private lateinit var register_btn: Button
    private lateinit var login_asGuest: Button

    private lateinit var webView_toolbar: Toolbar
    private lateinit var webViewBackBtn: ImageView

    override fun getLayoutId(): Int {
        return R.layout.fragment_welcome
    }

    override fun initializeViews() {
//        setLocale(getLoginActivity()?.getPreference()!!.get(app_language, getLocale()))
        tvLanguageSelector = tv_languageSelector

        login_btn = loginBtn
        register_btn = registerBtn
        login_asGuest = loginAsGuestBtn

        webView_toolbar = webViewToolbar
        webViewBackBtn = webView_backBtn
    }

    override fun setListeners() {
        tvLanguageSelector.setOnClickListener {
            if (getLoginActivity()?.getPreference()!!.get(app_language, "").equals(resources.getString(R.string.local_code_ar))) {
               getLoginActivity()?.getPreference()!!.put(app_language, resources.getString(R.string.local_code_en))
               setLocale(resources.getString(R.string.local_code_en))
           } else {
               getLoginActivity()?.getPreference()!!.put(app_language, resources.getString(R.string.local_code_ar))
               setLocale(resources.getString(R.string.local_code_ar))
           }
       }

        webViewBackBtn.setOnClickListener { getLoginActivity()?.onBackPressed() }

        login_btn.setOnClickListener {
            getLoginActivity()?.addFragment(
                LoginFragment(),
                addToBackStack = true
            )
        }
        login_asGuest.setOnClickListener {
            val guestProfile = Profile(UserFirstNameEN =  resources.getString(R.string.logged_in), UserLastNameEN = resources.getString(R.string.as_guest))
            val guestProfileJson = Gson().toJson(guestProfile)
            getLoginActivity()?.getPreference()?.put(Constants.profile, guestProfileJson)
            getLoginActivity()?.getPreference()?.put(Constants.asGuest, true)
            getLoginActivity()?.launchActivity<MainActivity>()
        }

        register_btn.setOnClickListener {
            openWebPage(welcomeWebViewLayout, welcome_webView, webURL, resources.getString(R.string.hukoomi_registration_link))
        }
    }
}