package qa.gov.hukoomi.hukoomimobile.login.activity

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_welcome.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.activity.BaseLoginActivity
import qa.gov.hukoomi.hukoomimobile.login.fragment.WelcomeFragment

class LoginActivity : BaseLoginActivity() {

    private lateinit var progressBar: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        progressBar = progressBarLayout

        addFragment(WelcomeFragment())
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun onBackPressed() {
        if(progressBar.visibility == View.VISIBLE) {
            hideLoading()
            return
        }
        if (welcomeWebViewLayout?.visibility == View.VISIBLE) {
            welcomeWebViewLayout?.visibility = View.GONE
        } else if (loginWebViewLayout?.visibility == View.VISIBLE) {
            loginWebViewLayout?.visibility = View.GONE
        } else {
            super.onBackPressed()
        }
    }
}
