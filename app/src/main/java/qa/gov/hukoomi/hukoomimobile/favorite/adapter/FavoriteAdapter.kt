package qa.gov.hukoomi.hukoomimobile.favorite.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_favorite.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.favorite.model.FavoriteModel

class FavoriteAdapter(val favoriteModel: ArrayList<FavoriteModel>,
                      val itemClick: ItemClick) :
    RecyclerView.Adapter<FavoriteAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var favoriteIcon = view.favoriteServiceIcon
        var favoriteName = view.favoriteServiceName
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_favorite, parent, false)

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Picasso
            .get()
            .load(favoriteModel.get(position).favoriteServiceIcon)
            .into(holder.favoriteIcon)

        holder.favoriteName.text = favoriteModel.get(position).favoriteServiceName

        holder.view.setOnClickListener { itemClick.itemClick(position, favoriteModel.get(position)) }
        holder.view.setOnLongClickListener { itemClick.itemLongClick(position, favoriteModel.get(position)) }
    }

    override fun getItemCount() = favoriteModel.size

    interface ItemClick {
        fun itemClick(position: Int, favoriteModel: FavoriteModel)
        fun itemLongClick(position: Int, favoriteModel: FavoriteModel): Boolean
    }
}