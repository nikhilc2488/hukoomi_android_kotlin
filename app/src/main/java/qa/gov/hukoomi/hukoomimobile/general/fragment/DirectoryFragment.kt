package qa.gov.hukoomi.hukoomimobile.general.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_directory.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import qa.gov.hukoomi.hukoomimobile.utils.inflate
import qa.gov.hukoomi.hukoomimobile.general.adapter.DirectoryAdapter
import qa.gov.hukoomi.hukoomimobile.general.model.DirectoryModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.directory_key

class DirectoryFragment : BaseFragment(), DirectoryAdapter.ItemClick {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_directory)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        innerPage = true
        getMainActivity().setServiceTitle(resources.getString(R.string.directory))
        getMainActivity().setServiceIcon(R.mipmap.ic_directory_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        val directoryList = ArrayList<DirectoryModel>()
        directoryList.add(DirectoryModel(R.mipmap.ic_ministries, "1", resources.getString(R.string.ministries)))
        directoryList.add(DirectoryModel(R.mipmap.ic_agencies,"5", resources.getString(R.string.agencies_and_organizations)))
        //directoryList.add(DirectoryModel(R.mipmap.ic_embassy,"0", resources.getString(R.string.embassies)))
        directoryList.add(DirectoryModel(R.mipmap.ic_education,"6", resources.getString(R.string.education_and_higer_education)))
        directoryList.add(DirectoryModel(R.mipmap.ic_tourism,"7", resources.getString(R.string.tourism_and_leisure)))
        directoryList.add(DirectoryModel(R.mipmap.ic_hospital,"8", resources.getString(R.string.hospitals_and_health_facilities)))
        directoryList.add(DirectoryModel(R.mipmap.ic_sports,"9", resources.getString(R.string.youth_and_sports)))
        directoryList.add(DirectoryModel(R.mipmap.ic_banks,"10", resources.getString(R.string.banks)))
        directoryList.add(DirectoryModel(R.mipmap.ic_insurance,"11", resources.getString(R.string.insurance_companies)))
        directoryList.add(DirectoryModel(R.mipmap.ic_charity,"12", resources.getString(R.string.charitable_organizations)))
        //directoryList.add(DirectoryModel(R.mipmap.ic_useful_numbers,"0", resources.getString(R.string.useful_numbers)))

        viewManager = LinearLayoutManager(getMainActivity().applicationContext)
        viewAdapter = DirectoryAdapter(directoryList, this)

        recyclerView = rv_general.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun item_click(position: Int, directory: DirectoryModel) {
        val bundle = Bundle()
        bundle.putParcelable(directory_key, directory)
        val generalItemFragment = DirectoryDetailFragment()
        generalItemFragment.arguments = bundle
        getMainActivity().addFragment(generalItemFragment)
    }
}