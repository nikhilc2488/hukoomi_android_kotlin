package qa.gov.hukoomi.hukoomimobile.general.model

import android.os.Parcel
import android.os.Parcelable

data class DirectoryDetailModel(val directoryType: String,
                        val name: String,
                        val address: String,
                        val email: String,
                        val phone: String,
                        val fax: String,
                        val website: String,
                        val image: String): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(directoryType)
        dest?.writeString(name)
        dest?.writeString(address)
        dest?.writeString(email)
        dest?.writeString(phone)
        dest?.writeString(fax)
        dest?.writeString(website)
        dest?.writeString(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DirectoryDetailModel> {
        override fun createFromParcel(parcel: Parcel): DirectoryDetailModel {
            return DirectoryDetailModel(parcel)
        }

        override fun newArray(size: Int): Array<DirectoryDetailModel?> {
            return arrayOfNulls(size)
        }
    }

}
