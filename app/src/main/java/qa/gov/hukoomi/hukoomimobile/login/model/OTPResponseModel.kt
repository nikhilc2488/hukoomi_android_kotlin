package qa.gov.hukoomi.hukoomimobile.login.model

data class OTPResponseModel(val errorCode: String,
                            val OTP_transactionID: String,
                            val description: String) {
}