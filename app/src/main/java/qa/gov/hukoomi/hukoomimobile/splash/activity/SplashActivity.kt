package qa.gov.hukoomi.hukoomimobile.splash.activity

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.activity.BaseLoginActivity
import qa.gov.hukoomi.hukoomimobile.login.activity.LoginActivity
import qa.gov.hukoomi.hukoomimobile.utils.*
import qa.gov.hukoomi.hukoomimobile.utils.Constants.app_language
import java.util.*

class SplashActivity : BaseLoginActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            //getPreference().put(app_language, deviceLanguage())
            //Log.d("Valueeee","sddsd"+getPreference(applicationContext).get(Constants.app_language,""))
            setLocale(getPreference(applicationContext).get(Constants.app_language,resources.getString(R.string.local_code_ar)), applicationContext)
            launchActivity<LoginActivity>()
        }, Constants.SPLASH_TIME_OUT)
    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

   /* fun deviceLanguage(): String {
        if (getPreference().get(app_language, "").equals(resources.getString(R.string.local_code_ar))) {
            setLocale(resources.getString(R.string.local_code_ar))
            return resources.getString(R.string.local_code_ar)
        } else {
            if (getLocale().equals(resources.getString(R.string.local_code_ar))) {
                setLocale(resources.getString(R.string.local_code_ar))
                return resources.getString(R.string.local_code_ar)
            } else {
                setLocale(resources.getString(R.string.local_code_en))
                return resources.getString(R.string.local_code_en)
            }
        }
    }*/

    /*fun setLocale(lang: String) {
        val myLocale = Locale(lang)
        val res = resources
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, null)
    }*/
}
