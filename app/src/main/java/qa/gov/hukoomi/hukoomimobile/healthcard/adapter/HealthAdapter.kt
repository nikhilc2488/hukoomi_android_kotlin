package qa.gov.hukoomi.hukoomimobile.healthcard.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_health_services.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardServiceModel

class HealthAdapter(
    private val healthCardModel: ArrayList<HealthCardServiceModel>,
    val itemClick: ItemClick
) :
    RecyclerView.Adapter<HealthAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var healthIcon = view.healthIcon
        var healthServiceText = view.healthServiceText
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_health_services, parent, false)

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Picasso
            .get()
            .load(healthCardModel.get(position).healthIcon)
            .into(holder.healthIcon)

        holder.healthServiceText.text = healthCardModel.get(position).healthServiceText

        holder.view.setOnClickListener { itemClick.itemClick(position, healthCardModel.get(position)) }
        holder.view.setOnLongClickListener { itemClick.itemLongClick(position, healthCardModel.get(position)) }
    }

    override fun getItemCount() = healthCardModel.size

    interface ItemClick {
        fun itemClick(position: Int, healthCardModel: HealthCardServiceModel)
        fun itemLongClick(position: Int, healthCardModel: HealthCardServiceModel): Boolean
    }
}