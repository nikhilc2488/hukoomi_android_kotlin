package qa.gov.hukoomi.hukoomimobile.healthcard.fragment

import android.os.Bundle
import android.support.v7.widget.CardView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fragment_health_card_renew_one.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardCheckExpiryModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import qa.gov.hukoomi.hukoomimobile.utils.Constants.cardExpiry
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import qa.gov.hukoomi.hukoomimobile.utils.inflate

class HealthCardRenewFragmentOne: BaseFragment(), View.OnClickListener {

    private lateinit var etQid: EditText
    private lateinit var check_card_renewal: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_health_card_renew_one)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        innerPage = true
        getMainActivity().setServiceTitle(resources.getString(R.string.health_card_renew))
        getMainActivity().setServiceIcon(R.mipmap.ic_health_card_renew_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        initializeViews()

    }

    fun initializeViews() {
        etQid = et_qid
        check_card_renewal = checkCardRenewalBtn
        check_card_renewal.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v) {
            check_card_renewal -> {
                if (TextUtils.isEmpty(etQid.text)) {
                    etQid.error = resources.getString(R.string.qid_number)
                    return
                }

                val bundle = Bundle()
                bundle.putParcelable(Constants.checkExpiryModel, HealthCardCheckExpiryModel(etQid.text.toString(), cardExpiry))
                val healthCardRenewTwo = HealthCardRenewFragmentTwo()
                healthCardRenewTwo.arguments = bundle

//                val bundle = Bundle()
//                bundle.putString(Constants.qid_number, etQid.text.toString())
//                val healthCardRenewTwo = HealthCardRenewFragmentTwo()
//                healthCardRenewTwo.arguments = bundle
                getMainActivity().addFragment(healthCardRenewTwo)
            }
        }
    }
}