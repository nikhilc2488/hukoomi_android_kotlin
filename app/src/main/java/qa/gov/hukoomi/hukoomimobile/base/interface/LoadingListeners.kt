package qa.gov.hukoomi.hukoomimobile.base.`interface`

interface LoadingListeners {

    fun showLoading()
    fun hideLoading()
}