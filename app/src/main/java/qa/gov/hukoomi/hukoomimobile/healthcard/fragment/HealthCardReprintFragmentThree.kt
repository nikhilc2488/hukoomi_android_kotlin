package qa.gov.hukoomi.hukoomimobile.healthcard.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_health_card_reprint_three.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardReprintModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.healthCardReprintModelKey
import qa.gov.hukoomi.hukoomimobile.utils.inflate

class HealthCardReprintFragmentThree: BaseFragment(), View.OnClickListener {

    private lateinit var qidNumber: TextView
    private lateinit var expiryDate: TextView
    private lateinit var nationality: TextView
    private lateinit var phone_number: TextView
    private lateinit var by_email: TextView
    private lateinit var email: TextView
    private lateinit var email_layout: LinearLayout
    private lateinit var sms_notification: TextView
    private lateinit var previous_btn: Button
    private lateinit var pay_btn: Button

    private lateinit var healthCardReprintModel: HealthCardReprintModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_health_card_reprint_three)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getMainActivity().setServiceTitle(resources.getString(R.string.health_card_reprint))
        getMainActivity().setServiceIcon(R.mipmap.ic_health_card_reprint_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        initializeViews()
        healthCardReprintModel = arguments?.getParcelable(healthCardReprintModelKey) as HealthCardReprintModel
        qidNumber.text = healthCardReprintModel.qidNumber
        expiryDate.text = healthCardReprintModel.expiryDate
        nationality.text = healthCardReprintModel.nationality.toString()
        phone_number.text = healthCardReprintModel.phoneNumber
        if (healthCardReprintModel.receiptByEmail == R.id.yes_byEmail) {
            by_email.text = resources.getString(R.string.yes)
            email_layout.visibility = View.VISIBLE
            email.text = healthCardReprintModel.email
        }
        if (healthCardReprintModel.smsNotification == R.id.yes_smsNotification)
            sms_notification.text = resources.getString(R.string.yes)
    }

    fun initializeViews() {
        qidNumber = qidNumber_stepThree
        expiryDate = expiryDate_stepThree
        nationality = nationality_stepThree
        phone_number = phoneNumber
        by_email = byEmail
        email_layout = emailLayout
        email = emailAddress
        sms_notification = smsNotification
        previous_btn = previousBtn_stepThree
        pay_btn = payBtn

        setListeners()
    }

    fun setListeners() {
        previous_btn.setOnClickListener(this)
        pay_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v) {
            previous_btn -> getMainActivity().onBackPressed()
            pay_btn -> {}
        }
    }
}