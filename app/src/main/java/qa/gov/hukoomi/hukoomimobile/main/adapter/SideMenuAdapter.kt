package qa.gov.hukoomi.hukoomimobile.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_side_menu.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.main.model.SideMenuModel

class SideMenuAdapter(private val sideMenuModel: ArrayList<SideMenuModel>,
                      val itemClick: ItemClick) :
    RecyclerView.Adapter<SideMenuAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var sideMenuIcon = view.sideMenuIcon
        var sideMenuName = view.sideMenuName
        var sideMenuLayout = view.sideMenuLayout
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        // create a new view
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_side_menu, parent, false)
        // set the view's size, margins, paddings and layout parameters

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        Picasso.get().load(sideMenuModel.get(position).sideMenuIcon).into(holder.sideMenuIcon)
        holder.sideMenuName.text = sideMenuModel.get(position).sideMenuName

        holder.sideMenuLayout.setOnClickListener { itemClick.itemClick(position, sideMenuModel.get(position)) }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = sideMenuModel.size

    interface ItemClick {
     fun itemClick(position: Int, sideMenuItem: SideMenuModel)
    }
}