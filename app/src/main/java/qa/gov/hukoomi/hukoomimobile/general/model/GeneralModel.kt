package qa.gov.hukoomi.hukoomimobile.general.model

data class GeneralModel(val generalItemIcon: Int,
                        val generalItemName: String) {
}