package qa.gov.hukoomi.hukoomimobile.base.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONException
import org.json.JSONObject
import qa.gov.hukoomi.hukoomimobile.login.activity.LoginActivity
import qa.gov.hukoomi.hukoomimobile.login.model.LoginResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.Profile
import qa.gov.hukoomi.hukoomimobile.utils.inflate
import java.util.*

abstract class BaseLoginFragment : Fragment() {

    abstract fun getLayoutId(): Int

    abstract fun initializeViews()
    abstract fun setListeners()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(getLayoutId())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initializeViews()
        setListeners()
    }

    fun getLoginActivity(): LoginActivity? {
        return activity as? LoginActivity
    }

    fun openWebPage(webViewLayout: LinearLayout, webView: WebView, webLink: TextView, webURL: String) {
        webViewLayout.visibility = View.VISIBLE
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(webURL)
        webLink?.text = "${webView.url}"
    }

    fun loadJSONFromAsset(): String {
        try {
            val inputStream = getLoginActivity()?.assets!!.open("login_response.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            val json = String(buffer)
            return json
        } catch (exception: JSONException) {
            exception.printStackTrace()
            return ""
        }
    }

    fun parseJSON(json: String): LoginResponseModel {

        val jsonObject = JSONObject(json)
        val loginResponseModel = LoginResponseModel(
            jsonObject.getString("errorCode"),
            Profile(
                jsonObject.getJSONObject("profile").getString("errorCode"),
                jsonObject.getJSONObject("profile").getString("AccountLastProfileSaveDate"),
                jsonObject.getJSONObject("profile").getString("AccountLevel"),
                jsonObject.getJSONObject("profile").getString("AccountType"),
                jsonObject.getJSONObject("profile").getString("StatusPhoneNumberVerification"),
                jsonObject.getJSONObject("profile").getString("UserAddressBuildingNumber"),
                jsonObject.getJSONObject("profile").getString("UserAddressPostOfficeBox"),
                jsonObject.getJSONObject("profile").getString("UserAddressStreetName"),
                jsonObject.getJSONObject("profile").getString("UserAddressZone"),
                jsonObject.getJSONObject("profile").getString("UserBirthdate"),
                jsonObject.getJSONObject("profile").getString("UserEmail"),
                jsonObject.getJSONObject("profile").getString("UserFirstNameAR"),
                jsonObject.getJSONObject("profile").getString("UserFirstNameEN"),
                jsonObject.getJSONObject("profile").getString("UserGender"),
                jsonObject.getJSONObject("profile").getString("UserLandLine"),
                jsonObject.getJSONObject("profile").getString("UserLastNameAR"),
                jsonObject.getJSONObject("profile").getString("UserLastNameEN"),
                jsonObject.getJSONObject("profile").getString("UserMiddleNameAR"),
                jsonObject.getJSONObject("profile").getString("UserMiddleNameEN"),
                jsonObject.getJSONObject("profile").getString("UserMobileNumber"),
                jsonObject.getJSONObject("profile").getString("UserNationality"),
                jsonObject.getJSONObject("profile").getString("UserQID"),
                jsonObject.getJSONObject("profile").getString("cn"),
                jsonObject.getJSONObject("profile").getString("UserEID")
            ),
            jsonObject.getString("ss"),
            ""
        )
        return loginResponseModel
    }

    fun setLocale(lang: String) {
        val myLocale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, null)
//        getPrefsHelper().putLocalLang(lang)

        val intent = Intent(getLoginActivity(), LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    fun getLocale(): String {
        return Locale.getDefault().language
    }
}