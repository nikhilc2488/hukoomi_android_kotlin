package qa.gov.hukoomi.hukoomimobile.login.fragment

import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_otp.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseLoginFragment
import qa.gov.hukoomi.hukoomimobile.base.helpers.UIHelper
import qa.gov.hukoomi.hukoomimobile.base.network.WebserviceFactory
import qa.gov.hukoomi.hukoomimobile.login.model.LoginResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.Profile
import qa.gov.hukoomi.hukoomimobile.login.model.VerifyOTPModel
import qa.gov.hukoomi.hukoomimobile.main.MainActivity
import qa.gov.hukoomi.hukoomimobile.utils.*
import qa.gov.hukoomi.hukoomimobile.utils.Constants.debug
import qa.gov.hukoomi.hukoomimobile.utils.Constants.transactionID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OTPFragment : BaseLoginFragment() {

    private lateinit var etOTP: EditText
    private lateinit var verify_btn: Button

    var transaction_id: String? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_otp
    }

    override fun initializeViews() {
        if (!debug) {
            transaction_id = arguments?.getString(transactionID)
        }

        etOTP = et_otp
        verify_btn = verifyBtn
    }

    override fun setListeners() {
        verify_btn.setOnClickListener {
            if (TextUtils.isEmpty(etOTP.text)) {
                etOTP.error = "Enter OTP"
                return@setOnClickListener
            }
            if (UIHelper().isNetworkAvailable(getLoginActivity()?.applicationContext)) {
                if (debug) {
                    val loginResponse = parseJSON(loadJSONFromAsset())
                    launchActivity(loginResponse.profile)
                } else {
                    getLoginActivity()?.showLoading()
                    verifyOTP(VerifyOTPModel(transaction_id!!, etOTP.text.toString()))
                }
            } else {
                getLoginActivity()?.hideLoading()
                getLoginActivity()?.toast(resources.getString(R.string.check_connection))
            }
        }
    }

    fun verifyOTP(verifyOTPModel: VerifyOTPModel) {
        val webService = WebserviceFactory()
        val callResponse = webService.verifyOTP(verifyOTPModel)

        callResponse.enqueue(object : Callback<LoginResponseModel> {
            override fun onFailure(call: Call<LoginResponseModel>, t: Throwable) {
                getLoginActivity()?.hideLoading()
                getLoginActivity()?.toast(t.message)
            }

            override fun onResponse(call: Call<LoginResponseModel>, response: Response<LoginResponseModel>) {
                getLoginActivity()?.hideLoading()
                if (response.isSuccessful) {
                    if (response.body()?.errorCode == "-1") {
                        Log.e("UserQID", response.body()?.profile?.UserQID)
                        val loginResponseProfile = response.body()?.profile
                        launchActivity(loginResponseProfile)
                    } else {
                        getLoginActivity()?.toast(response.body()?.error)
                    }
                }
            }
        })
    }

    fun launchActivity(profile: Profile?) {
        val profileJSON = Gson().toJson(profile)
        getLoginActivity()?.getPreference()?.put(Constants.profile, profileJSON)
        getLoginActivity()?.getPreference()?.put(Constants.asGuest, false)
        getLoginActivity()?.launchActivity<MainActivity>()
    }
}