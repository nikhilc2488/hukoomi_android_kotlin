package qa.gov.hukoomi.hukoomimobile.login.model

data class LoginModel(val username: String,
                      val password: String) {}