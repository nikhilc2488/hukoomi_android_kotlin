package qa.gov.hukoomi.hukoomimobile.general.fragment

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.fragment_general.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment
import qa.gov.hukoomi.hukoomimobile.favorite.model.FavoriteModel
import qa.gov.hukoomi.hukoomimobile.general.adapter.GeneralAdapter
import qa.gov.hukoomi.hukoomimobile.general.model.GeneralModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants

class GeneralFragment: BaseMainFragment(), GeneralAdapter.ItemClick {

    private lateinit var rvGeneral: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun getLayoutId(): Int {
        return R.layout.fragment_general
    }

    override fun initializeViews() {

        Constants.innerPage = true

        val generalList = ArrayList<GeneralModel>()
        generalList.add(GeneralModel(R.mipmap.ic_directory, resources.getString(R.string.directory)))

        viewManager = LinearLayoutManager(getMainActivity()?.applicationContext)
        viewAdapter = GeneralAdapter(generalList, this)

        rvGeneral = rv_general.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

        }
    }

    override fun setListeners() {
        //no listeners
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.general)
    }

    override fun serviceIcon(): Int {
        return R.mipmap.ic_general_title
    }

    override fun backBtnVisibility(): Int {
        return View.VISIBLE
    }

    override fun itemClick(position: Int, generalItem: GeneralModel) {
        when(position) {
            0 -> {
                getMainActivity()?.addFragment(DirectoryFragment())
            }
        }
    }

    override fun itemLongClick(position: Int, generalItem: GeneralModel): Boolean {
        when(position) {
            0 -> {
                val favoriteModel = FavoriteModel(generalItem.generalItemIcon, generalItem.generalItemName)
                return showDialog(favoriteModel)
            }
        }
        return false
    }
}