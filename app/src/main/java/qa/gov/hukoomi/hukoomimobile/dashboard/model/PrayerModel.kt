package qa.gov.hukoomi.hukoomimobile.dashboard.model

import java.util.*

data class PrayerModel(val remainingTime: String,
                       val nextPrayer: Date,
                       val nextPrayerName: String)

//data class PrayerModel(val date: String,
//                       val Fajr: String,
//                       val Sunrise: String,
//                       val Zuhr: String,
//                       val Asr: String,
//                       val Maghrib: String,
//                       val Isha: String)
//
//data class Day(val date: String,
//               val Fajr: String,
//               val Sunrise: String,
//               val Zuhr: String,
//               val Asr: String,
//               val Maghrib: String,
//               val Isha: String)

//data class PrayerModel(val code: Int,
//                       val status: String,
//                       val data: List<Data>)
//
//data class Data(val timings: Timings,
//                val date: Date)
//
//data class Timings(val Fajr: String,
//                   val Sunrise: String,
//                   val Dhuhr: String,
//                   val Asr: String,
//                   val Maghrib: String,
//                   val Isha: String) {}
//
//data class Date(val readable: String)