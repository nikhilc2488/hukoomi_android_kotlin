package qa.gov.hukoomi.hukoomimobile.base.activity

import android.content.Context
import android.content.SharedPreferences
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.`interface`.LoadingListeners
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import java.util.*

open abstract class BaseLoginActivity: AppCompatActivity(), LoadingListeners {

    private lateinit var sharedPreferences: SharedPreferences

    fun getPreference(): SharedPreferences {
        sharedPreferences = getSharedPreferences(Constants.pref_name, Context.MODE_PRIVATE)
        return sharedPreferences
    }

    fun addFragment(fragment: Fragment, addToBackStack: Boolean = false,
                    frameID: Int = R.id.frame_container) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(frameID, fragment)
        if (addToBackStack) { fragmentTransaction.addToBackStack(null) }
        fragmentTransaction.commit()
    }

    fun getLocale(): String {
        Log.e("localLanguage", Locale.getDefault().language)
        return Locale.getDefault().language
    }
}