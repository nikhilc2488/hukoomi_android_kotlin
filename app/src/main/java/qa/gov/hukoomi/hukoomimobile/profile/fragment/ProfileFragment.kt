package qa.gov.hukoomi.hukoomimobile.profile.fragment

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_profile.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseMainFragment
import qa.gov.hukoomi.hukoomimobile.login.model.Profile
import qa.gov.hukoomi.hukoomimobile.profile.adapter.ProfileAdapter
import qa.gov.hukoomi.hukoomimobile.profile.model.ProfileModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.profile
import qa.gov.hukoomi.hukoomimobile.utils.get

class ProfileFragment: BaseMainFragment() {

    private lateinit var rvProfile: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var userProfile: Profile

    override fun getLayoutId(): Int {
        return R.layout.fragment_profile
    }

    override fun initializeViews() {
        userProfile = Gson().fromJson(getMainActivity()?.getPreference()!!.get(profile, ""), Profile::class.java)

        val itemList = arrayListOf<ProfileModel>()
        itemList.add(ProfileModel(resources.getString(R.string.profile_qid_key), userProfile.UserQID))
        itemList.add(ProfileModel(resources.getString(R.string.profile_name_key), "${userProfile.UserFirstNameEN} ${userProfile.UserMiddleNameEN} ${userProfile.UserLastNameEN}"))
        itemList.add(ProfileModel(resources.getString(R.string.profile_email_key), userProfile.UserEmail))
        itemList.add(ProfileModel(resources.getString(R.string.profile_mobile_no_key), "+${userProfile.UserMobileNumber}"))
        itemList.add(ProfileModel(resources.getString(R.string.profile_birthday_key), userProfile.UserBirthdate))

        viewManager = LinearLayoutManager(getMainActivity()?.applicationContext)
        viewAdapter = ProfileAdapter(itemList)

        rvProfile = rv_profile.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

        }
    }

    override fun setListeners() {
        // no listeners
    }

    override fun serviceTitle(): String {
        return resources.getString(R.string.profile)
    }

    override fun serviceIcon(): Int {
        return R.mipmap.ic_profile_title
    }

    override fun backBtnVisibility(): Int {
        return View.VISIBLE
    }
}