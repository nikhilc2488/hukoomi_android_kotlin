package qa.gov.hukoomi.hukoomimobile.healthcard.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fragment_health_card_renew_three.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardRenewModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.healthCardRenewalModel
import qa.gov.hukoomi.hukoomimobile.utils.inflate

class HealthCardRenewFragmentThree: BaseFragment(), View.OnClickListener {

    private lateinit var qidNumber: TextView
    private lateinit var expiryDate: TextView
    private lateinit var num_of_years: TextView
    private lateinit var phone_number: TextView
    private lateinit var by_email: TextView
    private lateinit var sms_notification: TextView
    private lateinit var previous_btn: Button
    private lateinit var pay_btn: Button

    private lateinit var healthCardRenewModel: HealthCardRenewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_health_card_renew_three)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getMainActivity().setServiceTitle(resources.getString(R.string.health_card_renew))
        getMainActivity().setServiceIcon(R.mipmap.ic_health_card_renew_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        initializeViews()
        healthCardRenewModel = arguments?.getParcelable(healthCardRenewalModel) as HealthCardRenewModel
        qidNumber.text = healthCardRenewModel.qidNumber
        expiryDate.text = healthCardRenewModel.expiryDate
        num_of_years.text = healthCardRenewModel.numOfYears.toString()
        phone_number.text = healthCardRenewModel.phoneNumber
        if (healthCardRenewModel.receiptByEmail == R.id.yes_byEmail)
            by_email.text = resources.getString(R.string.yes)
        if (healthCardRenewModel.smsNotification == R.id.yes_smsNotification)
            sms_notification.text = resources.getString(R.string.yes)

    }

    fun initializeViews() {
        qidNumber = qidNumber_stepThree
        expiryDate = expiryDate_stepThree
        num_of_years = numOfYears
        phone_number = phoneNumber
        by_email = byEmail
        sms_notification = smsNotification
        previous_btn = previousBtn_stepThree
        pay_btn = payBtn

        setListeners()
    }

    fun setListeners() {
        previous_btn.setOnClickListener(this)
        pay_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v) {
            previous_btn -> getMainActivity().onBackPressed()
            pay_btn -> {}
        }
    }
}