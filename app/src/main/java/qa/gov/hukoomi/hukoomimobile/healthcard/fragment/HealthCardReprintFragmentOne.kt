package qa.gov.hukoomi.hukoomimobile.healthcard.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.fragment_health_card_reprint_one.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import qa.gov.hukoomi.hukoomimobile.utils.inflate

class HealthCardReprintFragmentOne: BaseFragment(), View.OnClickListener {

    private lateinit var etQid: EditText
    private lateinit var check_card_reprint: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_health_card_reprint_one)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        innerPage = true
        getMainActivity().setServiceTitle(resources.getString(R.string.health_card_reprint))
        getMainActivity().setServiceIcon(R.mipmap.ic_health_card_reprint_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        initializeViews()

    }

    fun initializeViews() {
        etQid = et_qid
        check_card_reprint = checkCardReprintBtn
        check_card_reprint.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v) {
            check_card_reprint -> {
                if (TextUtils.isEmpty(etQid.text)) {
                    etQid.error = resources.getString(R.string.qid_number)
                    return
                }

                val bundle = Bundle()
                bundle.putString(Constants.qid_number, etQid.text.toString())
                val healthCardReprintTwo = HealthCardReprintFragmentTwo()
                healthCardReprintTwo.arguments = bundle
                getMainActivity().addFragment(healthCardReprintTwo)
            }
        }
    }
}