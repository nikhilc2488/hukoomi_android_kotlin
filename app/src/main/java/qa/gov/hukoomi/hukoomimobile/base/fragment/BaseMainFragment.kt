package qa.gov.hukoomi.hukoomimobile.base.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import org.json.JSONException
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.favorite.model.FavoriteModel
import qa.gov.hukoomi.hukoomimobile.main.MainActivity
import qa.gov.hukoomi.hukoomimobile.utils.inflate
import qa.gov.hukoomi.hukoomimobile.utils.put
import qa.gov.hukoomi.hukoomimobile.utils.toast

abstract class BaseMainFragment: Fragment() {

    abstract fun getLayoutId(): Int

    abstract fun initializeViews()
    abstract fun setListeners()
    abstract fun serviceTitle(): String
    abstract fun serviceIcon(): Int
    abstract fun backBtnVisibility(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(getLayoutId())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getMainActivity()?.setServiceTitle(serviceTitle())
        getMainActivity()?.setServiceIcon(serviceIcon())
        getMainActivity()?.setBackBtnVisibility(backBtnVisibility())

        initializeViews()
        setListeners()
    }

    fun getMainActivity(): MainActivity? {
        return activity as? MainActivity
    }

    fun showDialog(favoriteModel: FavoriteModel): Boolean {
        val builder = AlertDialog.Builder(getMainActivity())
        builder.setTitle(resources.getString(R.string.add_to_favorites))
        builder.setMessage(resources.getString(R.string.add_to_favorites_message))

        builder.setPositiveButton(resources.getString(R.string.yes)){dialog, which ->
            val favoriteJSON = Gson().toJson(favoriteModel)
            getMainActivity()?.getPreference()?.put(favoriteModel.favoriteServiceName, favoriteJSON)
            getMainActivity()?.toast(resources.getString(R.string.service_added_to_favorites))
            dialog.dismiss()
        }

        builder.setNegativeButton(resources.getString(R.string.no)){dialog,which ->
            getMainActivity()?.getPreference()?.put(favoriteModel.favoriteServiceName, "")
            dialog.dismiss()
        }

        val dialog: AlertDialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.colorPrimary))
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.colorPrimary))
        }
        dialog.show()
        return true
    }

    fun loadJSONFromAssets(): String {
        try {
            val inputStream = getMainActivity()?.assets!!.open("DirectoryFile.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            val json = String(buffer)
            return json
        } catch (exception: JSONException) {
            exception.printStackTrace()
            return ""
        }
    }
}