package qa.gov.hukoomi.hukoomimobile.healthcard.model

data class HealthCardEServiceModel(var e_serviceIcon: Int,
                                   var e_serviceName: String) {
}