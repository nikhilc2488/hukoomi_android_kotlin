package qa.gov.hukoomi.hukoomimobile.utils

object Constants {

    val debug = true

    val base_url = "http://172.16.204.93:9000/api/"

    val requestCode_phoneCall_permission = 5001

    val SPLASH_TIME_OUT = 2000L

    var innerPage = false

    //Preferences Keys
    val pref_name = "Hukoomi-Mobile"
    val app_language = "app_language"
    val profile = "profile"
    val asGuest = "asGuest"
    val rememberQID = "rememberQID"
    val rememberMeCheck = "rememberMeCheck"

    //Bundle key for Login
    var transactionID = "transactionID"

    //Health Card Bundle Keys
    var qid_number = "qid_number"
    var checkExpiryModel = "checkExpiryModel"
    var healthCardRenewalModel = "healthCardRenewalModel"
    var healthCardReprintModelKey = "healthCardReprintModelKey"

    //Bundle keys for General
    var directory_key = "directory_key"

    var cardExpiry = ""
}