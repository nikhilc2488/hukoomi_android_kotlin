package qa.gov.hukoomi.hukoomimobile.healthcard.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fragment_health_card_reprint_two.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardReprintModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.healthCardReprintModelKey
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import qa.gov.hukoomi.hukoomimobile.utils.Constants.qid_number
import qa.gov.hukoomi.hukoomimobile.utils.inflate

class HealthCardReprintFragmentTwo: BaseFragment(), View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private lateinit var qidNumber: TextView
    private lateinit var expiryDate: TextView
    private lateinit var nationality: TextView
    private lateinit var etPhoneNumber: EditText
    private lateinit var byEmail_group: RadioGroup
    private lateinit var yesByEmail: RadioButton
    private lateinit var noByEmail: RadioButton
    private lateinit var etEmail: EditText
    private lateinit var email_layout: LinearLayout
    private lateinit var notification_group: RadioGroup
    private lateinit var yesSMSNotification: RadioButton
    private lateinit var noSMSNotification: RadioButton
    private lateinit var delivery_group: RadioGroup
    private lateinit var departmentDelivery: RadioButton
    private lateinit var postOfficeDelivery: RadioButton
    private lateinit var personalDelivery: RadioButton
    private lateinit var previous_btn: Button
    private lateinit var next_btn: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_health_card_reprint_two)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val bundle = arguments

        innerPage = true
        getMainActivity().setServiceTitle(resources.getString(R.string.health_card_reprint))
        getMainActivity().setServiceIcon(R.mipmap.ic_health_card_reprint_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        initializeViews()
        qidNumber.text = bundle?.getString(qid_number)
    }

    fun initializeViews() {
        qidNumber = qidNumber_stepTwo
        expiryDate = expiryDate_stepTwo
        nationality = nationality_stepTwo
        etPhoneNumber = et_phoneNumber
        byEmail_group = receiptByEmail_group
        yesByEmail = yes_byEmail
        noByEmail = no_byEmail
        email_layout = emailLayout
        etEmail = et_email
        notification_group = smsNotification_group
        yesSMSNotification = yes_smsNotification
        noSMSNotification = no_smsNotification
        delivery_group = deliveryOption_group
        departmentDelivery = department_delivery
        postOfficeDelivery = postOffice_delivery
        personalDelivery = personal_delivery
        previous_btn = previousBtn_stepTwo
        next_btn = nextBtn

        setListeners()
    }

    fun setListeners() {
        byEmail_group.setOnCheckedChangeListener(this)
        notification_group.setOnCheckedChangeListener(this)
        delivery_group.setOnCheckedChangeListener(this)
        previous_btn.setOnClickListener(this)
        next_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            previous_btn -> {
                getMainActivity().onBackPressed()
            }
            next_btn -> {
                if (TextUtils.isEmpty(etPhoneNumber.text)) {
                    etPhoneNumber.error = resources.getString(R.string.phone_number)
                    return
                }
                if (email_layout.visibility == View.VISIBLE) {
                    if (TextUtils.isEmpty(etEmail.text)) {
                        etEmail.error = resources.getString(R.string.email)
                        return
                    }
                }
                if (byEmail_group.checkedRadioButtonId == -1) {
                    Toast.makeText(
                        getMainActivity().applicationContext,
                        "${resources.getString(R.string.receive_receipts_by_email)}",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
                if (notification_group.checkedRadioButtonId == -1) {
                    Toast.makeText(
                        getMainActivity().applicationContext,
                        "${resources.getString(R.string.receive_sms_notification)}",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
                if (delivery_group.checkedRadioButtonId == -1) {
                    Toast.makeText(
                        getMainActivity().applicationContext,
                        "${resources.getString(R.string.delivery_options)} ?",
                        Toast.LENGTH_SHORT
                    ).show()
                    return
                }
                gotToStepThree()
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.yes_byEmail -> {
                email_layout.visibility = View.VISIBLE
            }
            R.id.no_byEmail -> {
                email_layout.visibility = View.GONE
            }
            R.id.yes_smsNotification -> {

            }
            R.id.no_smsNotification -> {

            }
            R.id.department_delivery -> {

            }
            R.id.postOffice_delivery -> {

            }
            R.id.personal_delivery -> {

            }
        }
    }

    fun gotToStepThree() {
        val healthCardReprintModel = HealthCardReprintModel(qidNumber.text.toString(),
            expiryDate.text.toString(), nationality.text.toString(), etPhoneNumber.text.toString(), byEmail_group.checkedRadioButtonId,
            etEmail.text.toString(), notification_group.checkedRadioButtonId, delivery_group.checkedRadioButtonId)

        val bundle = Bundle()
        bundle.putParcelable(healthCardReprintModelKey, healthCardReprintModel)
        val healthCardReprintThree = HealthCardReprintFragmentThree()
        healthCardReprintThree.arguments = bundle
        getMainActivity().addFragment(healthCardReprintThree)
    }
}