package qa.gov.hukoomi.hukoomimobile.about.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.row_about_us.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.about.model.AboutUsModel

class AboutUsAdapter(private val aboutUsModel: ArrayList<AboutUsModel>) :
    RecyclerView.Adapter<AboutUsAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var aboutUsTitle = view.aboutUs_itemTitle
        var aboutUsDescription = view.aboutUs_itemDescription
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        // create a new view
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_about_us, parent, false)
        // set the view's size, margins, paddings and layout parameters

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.aboutUsTitle.text = aboutUsModel.get(position).title
        holder.aboutUsDescription.text = aboutUsModel.get(position).description
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = aboutUsModel.size

}