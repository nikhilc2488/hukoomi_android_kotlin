package qa.gov.hukoomi.hukoomimobile.healthcard.model

import android.os.Parcel
import android.os.Parcelable

data class HealthCardRenewModel(val qidNumber: String,
                                val expiryDate: String,
                                val numOfYears: Int,
                                val phoneNumber: String,
                                val receiptByEmail: Int,
                                val smsNotification: Int) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(qidNumber)
        dest?.writeString(expiryDate)
        dest?.writeInt(numOfYears)
        dest?.writeString(phoneNumber)
        dest?.writeInt(receiptByEmail)
        dest?.writeInt(smsNotification)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HealthCardRenewModel> {
        override fun createFromParcel(parcel: Parcel): HealthCardRenewModel {
            return HealthCardRenewModel(parcel)
        }

        override fun newArray(size: Int): Array<HealthCardRenewModel?> {
            return arrayOfNulls(size)
        }
    }
}