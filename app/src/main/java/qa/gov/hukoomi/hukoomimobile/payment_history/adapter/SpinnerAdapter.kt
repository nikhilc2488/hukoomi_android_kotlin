package qa.gov.hukoomi.hukoomimobile.payment_history.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.row_spinner.view.*
import qa.gov.hukoomi.hukoomimobile.R

import java.util.ArrayList

class SpinnerAdapter(private val context: Context, val lookups_list: ArrayList<String>, private val localLang: String) :
    BaseAdapter() {

    override fun getCount(): Int {
        return lookups_list.size
    }

    override fun getItem(position: Int): Any {
        return lookups_list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var inflator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var view = inflator.inflate(R.layout.row_spinner, null)

        view.lookup.text = lookups_list.get(position)

//        if (localLang.equals(context.resources.getString(R.string.local_code_ar), ignoreCase = true)) {
//            holder.lookup!!.setText(lookups.getNameAr())
//        } else {
//            holder.lookup!!.setText(lookups.getNameEn())
//        }

        return view
    }

    internal inner class ViewHolder {
        var lookup: TextView? = null
    }
}
