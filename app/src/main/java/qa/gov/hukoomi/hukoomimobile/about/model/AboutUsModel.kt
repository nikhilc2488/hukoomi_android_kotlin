package qa.gov.hukoomi.hukoomimobile.about.model

data class AboutUsModel(val title: String,
                        val description: String) {
}