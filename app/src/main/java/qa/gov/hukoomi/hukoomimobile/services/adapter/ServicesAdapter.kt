package qa.gov.hukoomi.hukoomimobile.services.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.service_grid_row.view.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.services.model.ServiceModel

class ServicesAdapter(private val servicesModel: ArrayList<ServiceModel>,
                      val itemClick: ItemClick) :
    RecyclerView.Adapter<ServicesAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var serviceImage = view.serviceImage
        var serviceName = view.serviceName
        var disableItem = view.disableItem
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.service_grid_row, parent, false)

        return MyViewHolder(layout)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Picasso
            .get()
            .load(servicesModel.get(position).imageId)
            .into(holder.serviceImage)

        holder.serviceName.text = servicesModel.get(position).serviceName

        if (!servicesModel.get(position).disableOption) {
            holder.disableItem.visibility = View.GONE
        }

        holder.view.setOnClickListener { itemClick.itemClick(position, servicesModel.get(position)) }
    }

    override fun getItemCount() = servicesModel.size

    interface ItemClick {
     fun itemClick(position: Int, serviceModel: ServiceModel)
    }
}