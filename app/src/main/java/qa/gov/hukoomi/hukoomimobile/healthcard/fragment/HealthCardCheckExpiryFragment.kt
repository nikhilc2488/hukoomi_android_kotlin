package qa.gov.hukoomi.hukoomimobile.healthcard.fragment

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_health_card_check_expiry.*
import qa.gov.hukoomi.hukoomimobile.R
import qa.gov.hukoomi.hukoomimobile.base.fragment.BaseFragment
import qa.gov.hukoomi.hukoomimobile.utils.inflate
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.Window
import android.widget.*
import com.google.gson.Gson
import qa.gov.hukoomi.hukoomimobile.base.network.WebserviceFactory
import qa.gov.hukoomi.hukoomimobile.healthcard.model.Data
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardCheckExpiryModel
import qa.gov.hukoomi.hukoomimobile.healthcard.model.HealthCardCheckExpiryResponseModel
import qa.gov.hukoomi.hukoomimobile.login.model.Profile
import qa.gov.hukoomi.hukoomimobile.main.MainActivity
import qa.gov.hukoomi.hukoomimobile.utils.Constants
import qa.gov.hukoomi.hukoomimobile.utils.Constants.cardExpiry
import qa.gov.hukoomi.hukoomimobile.utils.Constants.checkExpiryModel
import qa.gov.hukoomi.hukoomimobile.utils.Constants.debug
import qa.gov.hukoomi.hukoomimobile.utils.Constants.innerPage
import qa.gov.hukoomi.hukoomimobile.utils.get
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class HealthCardCheckExpiryFragment : BaseFragment(), View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private lateinit var etQidNumber: EditText
    private lateinit var checkExpiry_btn: Button

    private lateinit var expiryResult_layout: LinearLayout
    //    private lateinit var expiryResult_text: TextView
    private lateinit var cardExpiryDate_text: TextView
    private lateinit var patient_id: TextView
    private lateinit var patient_name: TextView

    private lateinit var renewHealthCard_btn: Button

    private lateinit var errorResult_layout: LinearLayout
    private lateinit var errorResult_text: TextView

    private lateinit var qidGroup: RadioGroup
    private lateinit var myQIDRadio: RadioButton
    private lateinit var otherQIDRadio: RadioButton

    private lateinit var profileJSON: String
    private var profile: Profile? = null

    var healthCardCheckExpiryModel = HealthCardCheckExpiryModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_health_card_check_expiry)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        innerPage = true
        getMainActivity().setServiceTitle(resources.getString(R.string.health_card_check_expiry))
        getMainActivity().setServiceIcon(R.mipmap.ic_health_card_check_expiry_title)
        getMainActivity().setBackBtnVisibility(View.VISIBLE)

        initializeViews()
    }

    fun initializeViews() {
        etQidNumber = et_qid
        profileJSON = getMainActivity().getPreference().get(Constants.profile, "")
        profile = Gson().fromJson<Profile>(profileJSON, Profile::class.java)
        etQidNumber.setText(profile?.UserQID)
        checkExpiry_btn = checkExpiryBtn

        expiryResult_layout = expiryResultLayout
        cardExpiryDate_text = cardExpiryDate
        patient_id = patientID
        patient_name = patientName
        renewHealthCard_btn = renewHealthCardBtn

        errorResult_layout = errorResultLayout
        errorResult_text = errorResultText

        qidGroup = qid_group
        myQIDRadio = myQID_radio
        otherQIDRadio = otherQID_radio
        setListeners()
    }

    fun setListeners() {
        checkExpiry_btn.setOnClickListener(this)
        renewHealthCard_btn.setOnClickListener(this)
        renewHealthCard_btn.setOnClickListener(this)
        qidGroup.setOnCheckedChangeListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            checkExpiry_btn -> {
                expiryResult_layout.visibility = View.GONE
                errorResult_layout.visibility = View.GONE
                if (etQidNumber.length() < 11) {
                    etQidNumber.error = resources.getString(R.string.qid_must_be_number)
                    return
                }
                getMainActivity().showLoading()
                checkHealthCardExpiry(etQidNumber.text.toString())
            }
            renewHealthCard_btn -> {
                expiryResult_layout.visibility = View.GONE
                errorResult_layout.visibility = View.GONE
                val bundle = Bundle()
                bundle.putParcelable(checkExpiryModel, healthCardCheckExpiryModel)
                val healthCardRenew = HealthCardRenewFragmentTwo()
                healthCardRenew.arguments = bundle
                getMainActivity().addFragment(healthCardRenew)
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.myQID_radio -> {
                if (otherQIDRadio.isChecked) {
                    otherQIDRadio.isChecked = false
                }
                etQidNumber.setText(profile?.UserQID)
            }
            R.id.otherQID_radio -> {
                if (myQIDRadio.isChecked) {
                    myQIDRadio.isChecked = false
                }
                etQidNumber.text.clear()
            }
        }
    }

    fun checkHealthCardExpiry(qidNumber: String) {
        healthCardCheckExpiryModel.QID = etQidNumber.text.toString()
        if (debug) {
            getMainActivity().hideLoading()
            showResultLayout(Data("", "", "", "1234567", "NAME", "", "", "31-12-2018", ""))
        } else {
            val webService = WebserviceFactory()
            val callResponse = webService.checkHealthCardExpiry(HealthCardCheckExpiryModel(qidNumber))

            callResponse.enqueue(object : Callback<HealthCardCheckExpiryResponseModel> {
                override fun onFailure(call: Call<HealthCardCheckExpiryResponseModel>, t: Throwable) {
                    getMainActivity().hideLoading()
                    Toast.makeText(
                        getMainActivity().applicationContext,
                        t.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onResponse(
                    call: Call<HealthCardCheckExpiryResponseModel>,
                    response: Response<HealthCardCheckExpiryResponseModel>
                ) {
                    getMainActivity().hideLoading()
                    if (response.isSuccessful) {
                        if (response.body()?.errorCode == "-1" && response.body()!!.status) {
                            showResultLayout(response.body()?.data)
                            healthCardCheckExpiryModel.expiryDate = response.body()?.data?.cardExpiryDate.toString()
                        } else {
                            errorResult_layout.visibility = View.VISIBLE
                            errorResult_text.text = response.body()?.errors?.get(0)?.msg
//                        Toast.makeText(
//                            getMainActivity().applicationContext,
//                            response.body()?.errors?.get(0)?.msg,
//                            Toast.LENGTH_SHORT
//                        ).show()
                        }
                    }
                }
            })
        }
    }

    fun showResultLayout(data: Data? = null) {
        expiryResult_layout.visibility = View.VISIBLE
        healthCardCheckExpiryModel.expiryDate = data?.cardExpiryDate.toString()
        cardExpiry = data?.cardExpiryDate.toString()

        val current_date = SimpleDateFormat("dd-MM-yyyy").parse(getCurrentDate())
        val expiry_date = SimpleDateFormat("dd-MM-yyyy").parse(data?.cardExpiryDate)

        patient_id.text = data?.patientID
        patient_name.text = data?.holderFullNameEN
        cardExpiryDate_text.text = data?.cardExpiryDate
        if (current_date.compareTo(expiry_date) > 0) {
//            expiryResult_text.text = resources.getString(R.string.your_health_card_has_expired_on)
            cardExpiryDate_text.setTextColor(getMainActivity().resources.getColor(R.color.colorPrimary))
        } else {
//            expiryResult_text.text = resources.getString(R.string.your_health_card_will_expire_on)
            cardExpiryDate_text.setTextColor(getMainActivity().resources.getColor(R.color.due_payment_text_color))
        }
    }

    fun getCurrentDate(): String {
        val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
        val currentDate = simpleDateFormat.format(Date())
        return currentDate
    }

//    fun showDialog(expiryDate: String? = null) {
//        val dialog = Dialog(getMainActivity())
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setCancelable(true)
//        dialog.setCanceledOnTouchOutside(false)
//        dialog.getWindow().setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
//        dialog.setContentView(R.layout.dialog_health_card_expiry)
//        dialog.show()
//
//        var dialogClose = dialog.dialog_close
//        var card_expiryDate = dialog.cardExpiryDate
//        card_expiryDate.text = expiryDate
//        var renew_healthCardBtn = dialog.renewHealthCardBtn
//
//        dialogClose.setOnClickListener { dialog.dismiss() }
//        renew_healthCardBtn.setOnClickListener {
//            dialog.dismiss()
//            getMainActivity().addFragment(HealthCardRenewFragmentOne(), frameID = R.id.main_frame_container)
//        }
//    }
}