package qa.gov.hukoomi.hukoomimobile.general

import org.json.JSONObject

/**
 * Created by Nikhil Chindarkar on 08-01-2019.
 * Malomatia India Pvt. Ltd
 */
class APIController constructor(serviceInjection: ServiceInterface): ServiceInterface {
    private val service: ServiceInterface = serviceInjection

    override fun post(path: String, params: JSONObject, completionHandler: (response: JSONObject?) -> Unit) {
        service.post(path, params, completionHandler)
    }
}