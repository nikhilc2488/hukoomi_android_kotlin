package qa.gov.hukoomi.hukoomimobile.favorite.model

data class FavoriteModel(val favoriteServiceIcon: Int,
                         val favoriteServiceName: String) {
}