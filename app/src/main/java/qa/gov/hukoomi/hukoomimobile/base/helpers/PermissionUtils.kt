package qa.gov.hukoomi.hukoomimobile.base.helpers

import android.Manifest.permission.CALL_PHONE
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import qa.gov.hukoomi.hukoomimobile.utils.Constants.requestCode_phoneCall_permission

class PermissionUtils {

    fun requestPhoneCallPermission(activity: Activity): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }
        if (ContextCompat.checkSelfPermission(activity, CALL_PHONE)
            == PackageManager.PERMISSION_GRANTED) {
            return true
        }

        ActivityCompat.requestPermissions(activity, arrayOf(CALL_PHONE), requestCode_phoneCall_permission)
        return false
    }
}